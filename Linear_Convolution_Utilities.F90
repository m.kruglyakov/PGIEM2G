MODULE LINEAR_CONVOLUTION_UTILITIES
#ifndef OLD_CONVOLUTION
    USE HANKEL_TRANSFORM_UTILITIES
    IMPLICIT NONE

    INCLUDE "CONV_WEIGHTS.F90"

    INTEGER::FILTER_BOUNDS(2)
#ifdef LONGTRANSFORM
    REAL(DP),PARAMETER:: INPUT_FUNC(2048)=INPUT_FUNC_01 
    REAL(DP),PARAMETER:: YBASE(2048)=LMS_01

    DATA FILTER_BOUNDS / 0,2047 /
#else
    REAL(DP),PARAMETER:: INPUT_FUNC(1024)=INPUT_FUNC_02
    REAL(DP),PARAMETER:: YBASE(1024)=LMS_02
    DATA FILTER_BOUNDS / 256,768 /
#endif
    INTEGER::F_SHIFT
    LOGICAL::SHIFT_INPUT
    DATA SHIFT_INPUT /.FALSE./

CONTAINS
#ifdef GURU_INTERFACE_ENABLE
    SUBROUTINE SET_FILTER_BOUNDS(bnd)
        INTEGER,INTENT(IN)::bnd(2)
        FILTER_BOUNDS=bnd
    ENDSUBROUTINE
    SUBROUTINE GET_FILTER_BOUNDS(bnd)
        INTEGER,INTENT(OUT)::bnd(2)
        bnd=FILTER_BOUNDS
    ENDSUBROUTINE
#endif
    SUBROUTINE InitializeConvolution(s,px,py,f,l,lms,norms) !NOT THREAD SAFETY!
        REAL(RealParm2),INTENT(IN)::s
        INTEGER,INTENT(IN)::px,py
        INTEGER,INTENT(IN)::f,l
        REAL(RealParm2),INTENT(OUT)::lms(f:l)
        REAL(RealParm2),INTENT(OUT)::norms
        REAL(DP)::step,r,s1
#ifdef LONGTRANSFORM
        lms=LMS_01(f:l)
        step=0.1_DP
#else
        lms=LMS_02(f:l)
        step=0.2_DP
#endif
        FILTER_BEGIN=FILTER_BOUNDS(1)
        FILTER_END=FILTER_BOUNDS(2)
        IF (SHIFT_INPUT) THEN
            F_SHIFT=INT(LOG(s)/step)
            s1=EXP(F_SHIFT*step)
            norms=s/s1
            lambda_shift=0
        ELSE
            F_SHIFT=0
            s1=s
            norms=1
            lms=lms/s1
            lambda_shift=LOG(s1)
        ENDIF
        FILTER_BEGIN=MAX(FILTER_BEGIN-F_SHIFT,0)
        FILTER_END=MIN(FILTER_END-F_SHIFT,N-1)

    END SUBROUTINE
    FUNCTION    FINAL_CONVOLUTION(OUT_FUNC,IN_FUNC,f,l,SMALLEST_INDEX,LARGEST_INDEX) RESULT(RES)
        REAL(DP), INTENT(IN)::OUT_FUNC(1:,1:,1:)
        REAL(DP), INTENT(IN)::IN_FUNC(1:)
        INTEGER,  INTENT(IN)::f,l
        INTEGER,  INTENT(IN)::SMALLEST_INDEX,LARGEST_INDEX
        REAL(DP)::RES(6,SIZE(OUT_FUNC,2),f:l)
        INTEGER::I,J,N2,K,Kmin,Kmax
        REAL(DP)::s(SIZE(OUT_FUNC,1),SIZE(OUT_FUNC,2))
        REAL(DP)::w
        N2=N/2
        Kmin=N+1
        Kmax=-1
        RES=0
        
        DO I=N2+f,N2+l  
            s=0
            DO J=MAX(SMALLEST_INDEX+1,I-FILTER_END+1),MIN(I-FILTER_BEGIN+1,LARGEST_INDEX)
                K=I-J+1
                    w=IN_FUNC(K)
                    s=s+w*OUT_FUNC(:,:,J)
            ENDDO
            RES(:,:,I-N2)=s
        ENDDO
    ENDFUNCTION
#endif
ENDMODULE
