!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE APQ_Module
	USE CONST_MODULE
	USE DATA_TYPES_MODULE
	IMPLICIT NONE
	INTERFACE Calc_pexpz
		MODULE PROCEDURE Calc_pexpz_All,Calc_pexpz_Seg
	END INTERFACE
	INTERFACE Calc_qexpz
		MODULE PROCEDURE Calc_qexpz_All,Calc_qexpz_Seg
	END INTERFACE

        INTERFACE EXP
                MODULE PROCEDURE EXP_DIM
        ENDINTERFACE
        PRIVATE :: EXP
CONTAINS
	SUBROUTINE Calc_pexpz_All(bkg,anomaly,expz,eta,p,pexpz)
		TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
		TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
		COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),p(bkg%Nl,2),eta(bkg%Nl)
		COMPLEX(REALPARM),INTENT(INOUT)::pexpz(2,anomaly%Nz)
		COMPLEX(REALPARM)::e,e1,e2,w,a(2),b(2),c(2),d(2)
		INTEGER::I,l
		l=anomaly%Lnumber(0)
		IF (l>1) THEN
			e=EXP(2e0_REALPARM*eta(l), anomaly%z(0),bkg%depth(l-1))
		ELSEIF(l==1) THEN
			e=EXP(2e0_REALPARM*eta(1), anomaly%z(0), R_ZERO)
		ELSE
			e=C_ONE
		ENDIF
		
		DO I=1,anomaly%Nz
			IF (l/=anomaly%Lnumber(I)) THEN
				e=C_ONE
			ENDIF
			l=anomaly%Lnumber(I)
			a=e*p(l,:)
			pexpz(:,I)=a
			e=e*expz(I)*expz(I)
		ENDDO
        ENDSUBROUTINE
	SUBROUTINE Calc_qexpz_All(bkg,anomaly,expz,eta,q,qexpz)
		TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
		TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
		COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),q(bkg%Nl,2),eta(bkg%Nl)
		COMPLEX(REALPARM),INTENT(INOUT)::qexpz(2,anomaly%Nz)
		COMPLEX(REALPARM)::e,e1,e2,w,a(2),b(2),c(2),d(2)
		INTEGER::I,l
		l=anomaly%Lnumber(anomaly%Nz)
		IF (l/=bkg%Nl) THEN
			e=EXP(2e0_REALPARM*eta(l), bkg%depth(l), anomaly%z(anomaly%Nz))
		ELSE
			e=C_ZERO
		ENDIF
		DO I=anomaly%Nz,1,-1
			IF (l/=anomaly%Lnumber(I)) THEN
				e=C_ONE
			ENDIF
			l=anomaly%Lnumber(I)
			a=e*q(l,:)
			qexpz(:,I)=a
			e=e*expz(I)*expz(I)
		ENDDO
	ENDSUBROUTINE
	SUBROUTINE Calc_pexpz_Seg(bkg,anomaly,expz,eta,p,N1,N2,pexpz)
		TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
		TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
		COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),p(bkg%Nl,2),eta(bkg%Nl)
		INTEGER,INTENT(IN)::N1,N2
		COMPLEX(REALPARM),INTENT(INOUT)::pexpz(2,1:anomaly%Nz)
		COMPLEX(REALPARM)::e,e1,e2,w,a(2),b(2),c(2),d(2)
		INTEGER::I,l
		l=anomaly%Lnumber(N1-1)
		IF (l>1) THEN
			e=EXP(2e0_REALPARM*eta(l), anomaly%z(N1-1), bkg%depth(l-1))
		ELSEIF(l==1) THEN
			e=EXP(2e0_REALPARM*eta(1), anomaly%z(N1-1), R_ZERO)
		ELSE
			e=C_ONE
		ENDIF
		
		DO I=N1,N2
			IF (l/=anomaly%Lnumber(I)) THEN
				e=C_ONE
			ENDIF
			l=anomaly%Lnumber(I)
			a=e*p(l,:)
			pexpz(:,I)=a
			e=e*expz(I)*expz(I)
		ENDDO
	ENDSUBROUTINE

	SUBROUTINE Calc_qexpz_Seg(bkg,anomaly,expz,eta,q,N1,N2,qexpz)
		TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
		TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
		COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),q(bkg%Nl,2),eta(bkg%Nl)
		INTEGER,INTENT(IN)::N1,N2
		COMPLEX(REALPARM),INTENT(INOUT)::qexpz(2,1:anomaly%Nz)
		COMPLEX(REALPARM)::e,e1,e2,w,a(2),b(2),c(2),d(2)
		INTEGER::I,l
		l=anomaly%Lnumber(N2)
		IF (l/=bkg%Nl) THEN
			e=EXP(2e0_REALPARM*eta(l), bkg%depth(l), anomaly%z(N2))
		ELSE
			e=C_ZERO
		ENDIF
		DO I=N2,N1,-1
			IF (l/=anomaly%Lnumber(I)) THEN
				e=C_ONE
			ENDIF
			l=anomaly%Lnumber(I)
			a=e*q(l,:)
			qexpz(:,I)=a
			e=e*expz(I)*expz(I)
		ENDDO
	ENDSUBROUTINE
	SUBROUTINE Calc_Apq(bkg,lms,Arr,p,q,eta,A)
		TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
		COMPLEX(REALPARM),INTENT(INOUT)::p(bkg%Nl,2),q(bkg%Nl,2)
		COMPLEX(REALPARM),INTENT(INOUT)::eta(bkg%Nl),Arr(bkg%Nl,2)
		COMPLEX(REALPARM),OPTIONAL,INTENT(INOUT)::A(bkg%Nl,bkg%Nl,2)
		REAL(REALPARM),INTENT(IN)::lms
		COMPLEX(REALPARM)::eta0,el(bkg%Nl),w(bkg%Nl,2),tmp
		INTEGER:: I,J
		DO I=1,bkg%Nl
			eta(I)=SQRT(lms*lms-bkg%k2(I))
		ENDDO
		DO I=1,bkg%Nl-1
			el(I)=EXP(-R_TWO*eta(I)*bkg%thick(I))
		ENDDO
		el(bkg%Nl)=C_ZERO
		p=Calc_p(bkg,eta,el,lms)
		q=Calc_q(bkg,eta,el)
		DO I=1,bkg%Nl
			Arr(I,:)=C_ONE/eta(I)/(C_ONE-p(I,:)*q(I,:)*el(I))
		ENDDO
		IF (PRESENT(A))THEN
			DO I=1,bkg%Nl-1
				tmp=(bkg%k2(I)-bkg%k2(I+1))
				tmp=bkg%depth(I)*tmp
				tmp=tmp/(eta(I+1)+eta(I))
				w(I,:)=(C_ONE+p(I+1,:))/(C_ONE+p(I,:)*el(I))*EXP(tmp)
			ENDDO
			DO I=1,bkg%Nl
				A(I,I,:)=Arr(I,:)
				DO J=I-1,1,-1
					A(J,I,:)=A(J+1,I,:)*w(J,:)
					A(I,J,1)=A(J,I,1)
					A(I,J,2)=A(J,I,2)*bkg%csigma(I)/bkg%csigma(J)
				ENDDO
			ENDDO
		ENDIF
	ENDSUBROUTINE
	FUNCTION Calc_p(bkg,eta,el,lms) RESULT(p)
			TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
			COMPLEX(REALPARM),INTENT(IN)::eta(bkg%Nl),el(bkg%Nl)
			REAL(REALPARM),INTENT(IN)::lms
			COMPLEX(REALPARM)::p(bkg%Nl,2)
			COMPLEX(REALPARM)::eta0,w(2),a(2),cgamma,ceta
			INTEGER:: I
			eta0=sqrt(lms*lms-bkg%k2(0))
			ceta=eta0/eta(1)
			p(1,1)=(C_ONE-ceta)/(C_ONE+ceta)
			cgamma=bkg%csigma(0)/bkg%csigma(1)

			p(1,2)=(cgamma-ceta)/(cgamma+ceta)

			DO I=2,bkg%Nl
				w(1)=el(I-1)*p(I-1,1)
				w(2)=el(I-1)*p(I-1,2)
				w=(w-1)/(w+1)
				a(1)=eta(I-1)/eta(I)
				a(2)=a(1)*bkg%csigma(I)/bkg%csigma(I-1)
				w=w*a
				p(I,:)=(1+w)/(1-w)
			ENDDO
	END FUNCTION
	FUNCTION Calc_q(bkg,eta,el) RESULT(q)
			TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
			COMPLEX(REALPARM),INTENT(IN)::eta(bkg%Nl),el(bkg%Nl)
			COMPLEX(REALPARM)::q(bkg%Nl,2)
			COMPLEX(REALPARM)::eta0,w(2),a(2)
			INTEGER:: I
			q(bkg%Nl,:)=C_ZERO
			IF (bkg%Nl>1) THEN 
				a(1)=eta(bkg%Nl)/eta(bkg%Nl-1)
				a(2)=a(1)*bkg%csigma(bkg%Nl-1)/bkg%csigma(bkg%Nl)
				q(bkg%Nl-1,:)=(1-a)/(1+a)
				DO I=bkg%Nl-2,1,-1
					w(1)=el(I+1)*q(I+1,1)
					w(2)=el(I+1)*q(I+1,2)
					w=(w-1)/(w+1)
					a(1)=eta(I+1)/eta(I)
					a(2)=a(1)*bkg%csigma(I)/bkg%csigma(I+1)
					w=w*a
					q(I,:)=(1+w)/(1-w)
				ENDDO
			ENDIF
	END FUNCTION

        ELEMENTAL FUNCTION EXP_DIM(eta, d2, d1) RESULT(RES) !EXP(-eta*(d2-d1)
                COMPLEX(REALPARM), INTENT(IN) :: eta
                REAL(REALPARM), INTENT(IN) :: d1, d2
                COMPLEX(REALPARM) :: RES
                RES=EXP(-eta*DIM(d2,d1))
        ENDFUNCTION
END MODULE
