
FFT_QUAD_OOURA="YES"

ifeq ($(PLATFORM), GNU_PUHTI)
FC=mpifort
F77=mpifort
FC_Link=mpifort

FOPTS= -g -flto -fopenmp -O3

FOPTS+= -D no_FFTW_MPI

FFTW_INC=$(FFTW_INSTALL_ROOT)/include/
LIB_FFTW=$(FFTW_INSTALL_ROOT)/lib -lfftw3_omp -lfftw3
#LIB_BLAS=$(HOME)/privatemodules/OpenBLAS/lib -lopenblas
LIB_BLAS=/appl/spack/install-tree/gcc-9.1.0/openblas-0.3.12-qcx6fy/lib -lopenblas

Ar=ar


endif


ifeq ($(PLATFORM), GNU_EULER)
FC=mpifort
F77=mpifort
FC_Link=mpifort

FOPTS= -g  -flto  -fopenmp  -O3   -D LEGACY_MPI 


FFTW_INC=$(FFTW3_ROOT_DIR)/include/
LIB_FFTW=$(FFTW3_ROOT_DIR)/lib  -lfftw3_omp -lfftw3_mpi  -lfftw3 
LIB_BLAS=$(GOTOBLAS2)/lib  -lopenblas


Ar=ar

endif




ifeq ($(PLATFORM), BGP_FAST)

FC= mpixlf2003_r
F77= mpixlf77_r
FC_Link=mpixlf2003_r
AR=ar

FFT_QUAD_OOURA="NO"
FFLAGS= -qessl -g -O2 -qstrict  -qsmp=omp   -qalias=std -qarch=450 -qhot -qmaxmem=-1  -qipa=partition=large

FDEFS=-WF,-DLEGACY_MPI -WF,-DIBM_Bluegene  -WF,-DMPI_TIMER  -WF,-Dinternal_timer  -WF,-DNO_MEMORY_CHECKING 
FOPTS=$(FFLAGS) $(FDEFS)

LIB_FFTW=/gpfs/opt/bgp/fftw/3.3.3-stable/lib -lfftw3_mpi  -lfftw3_omp   -lfftw3
FGMRES_PATH=/gpfs/data/kruglyakov/lib/src/FGMRES_original
LIB_BLAS=/opt/ibmmath/lib  -lesslsmpbg 
LIB_ADD=-L$(HOME)/lib/lib/lib -lzspmv 

#-L/opt/ibmcmp/xlmass/bg/11.1/bglib -lmass -lmassv

INCLUDE= -I/opt/ibmmath/include -I/opt/ibmcmp/xlmass/bg/4.4/include -I/gpfs/opt/bgp/fftw/3.3.3-stable/include
BLAS_INC=-I/opt/ibmmath/include -I/opt/ibmcmp/xlmass/bg/4.4/include 
FFTW_INC=/gpfs/opt/bgp/fftw/3.3.3-stable/include
INSTALL_PATH=~/bin/fast/giem2g_master_solve_p_2020

endif

ifeq ($(PLATFORM), BGP_TEST)

FC= mpixlf2003_r
F77= mpixlf77_r
FC_Link=mpixlf2003_r
AR=ar

FFT_QUAD_OOURA="NO"
FFLAGS= -qessl -g -O0  -qsmp=omp -qarch=450    -qmaxmem=-1 -qcheck 
FDEFS=-WF,-DLEGACY_MPI -WF,-DIBM_Bluegene  -WF,-DMPI_TIMER  -WF,-Dinternal_timer -WF,-DNO_MEMORY_CHECKING  
FOPTS=$(FFLAGS) $(FDEFS)

LIB_FFTW=/gpfs/opt/bgp/fftw/3.3.3-stable/lib -lfftw3_mpi  -lfftw3_omp   -lfftw3
FGMRES_PATH=/gpfs/data/kruglyakov/lib/src/FGMRES_original
LIB_BLAS=/opt/ibmmath/lib  -lesslsmpbg 
LIB_ADD=-L$(HOME)/lib/lib/lib -lzspmv 

BLAS_INC=-I/opt/ibmmath/include -I/opt/ibmcmp/xlmass/bg/4.4/include 
FFTW_INC=/gpfs/opt/bgp/fftw/3.3.3-stable/include

INSTALL_PATH=~/bin/test/giem2g_test_2020
endif


ifeq ($(PLATFORM), EULER_INTEL)
FC=mpiifort
F77=mpiifort
FC_Link=mpiifort

#FOPTS= -g  -Bstatic  -qopenmp  -i4   -O3    -xHost -prec-div -inline speed -ipo  -D NOT_USE_OMP_LIB
FOPTS= -g    -qopenmp    -O3     -ipo  -D NOT_USE_OMP_LIB
FOPTS+= -D no_FFTW_MPI


FFTW_INC=$(FFTW3_ROOT_DIR)/include/
LIB_FFTW=$(FFTW3_ROOT_DIR)/lib64   -lfftw3_omp -lfftw3 
LIB_BLAS= $(MKLROOT)/lib/intel64 -lmkl_sequential  -lmkl_intel_lp64 -lmkl_core

LIBS+= -L$(LIB_BLAS)
LIBS+=  -L$(LIB_FFTW)
INCLUDE= -I.  -I$(FFTW_INC)

DYN=-Bdynamic
AR=xiar
INSTALL_PATH=~/bin/GIEM2G/giem2g_intel
endif



