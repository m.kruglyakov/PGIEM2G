
VMATCH_SRC = $(wildcard generated/Vertical/*match*.F90) $(wildcard generated/Vertical/*Frames*.F90)
VMATCH = $(patsubst %.F90,%.o,$(VMATCH_SRC))

VERTICAL_INT_SRC = $(wildcard generated/Vertical/*expansion*.F90) 
VERTICAL_INT = $(patsubst %.F90,%.o,$(VERTICAL_INT_SRC)) 
