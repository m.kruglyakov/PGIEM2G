#define _SUBNAME CONCAT(CALC_OUTPUT_FUNC_INT2,_FUNC)
#define _QQ0_COEFF CONCAT(QQ0,_FUNC)
#define _QQ1_COEFF CONCAT(QQ1,_FUNC)

#define _QQ2_A CONCAT(QQ2_A,_FUNC)
#define _QQ3_A CONCAT(QQ3_A,_FUNC)
#define _QQ4_A CONCAT(QQ4_A,_FUNC)

#define _QQ2_B CONCAT(QQ2_B,_FUNC)
#define _QQ3_B CONCAT(QQ3_B,_FUNC)
#define _QQ4_B CONCAT(QQ4_B,_FUNC)

#define _PP0_0 CONCAT(PP0_0,_FUNC)
#define _PP0_1 CONCAT(PP0_1,_FUNC)

#define _PP1 CONCAT(PP1,_FUNC)
#define _PP2 CONCAT(PP2,_FUNC)

#define _N_COEFFS CONCAT(N_COEFFS,_FUNC)
#define _N_COEFFS_LEN CONCAT(N_COEFFS_LEN,_FUNC)

#define _IS_EVEN CONCAT(IS_EVEN,_FUNC)

#define _HPOW0 CONCAT(POW_AT_ZERO,_FUNC)
#define _HPOW  CONCAT(POW,_FUNC)
#define _TPOW  CONCAT(TAIL_POW,_FUNC)
#define _FRAC_MULT  CONCAT(FRAC_MULT,_FUNC)

SUBROUTINE  _SUBNAME (l,X,MFUNCS,N1,N2,RES) 
    INTEGER , INTENT(IN)   :: l 
    REAL(DP), INTENT(IN)   :: X(0:N-1) 
    REAL(DP), INTENT(IN)   :: MFUNCS(6,0:N-1) 
    INTEGER,  INTENT(IN)   :: N1,N2
    REAL(DP), INTENT(INOUT):: RES(0:N-1)
    REAL(DP)::QQ0(0:SIZE(_QQ0_COEFF,1)-1)
    REAL(DP)::QQ1(0:SIZE(_QQ1_COEFF,1)-1)

    REAL(DP)::QQ2(0:MAX(SIZE(_QQ2_A,1),SIZE(_QQ2_B,1))-1)
    REAL(DP)::QQ3(0:MAX(SIZE(_QQ3_A,1),SIZE(_QQ3_B,1))-1)
    REAL(DP)::QQ4(0:MAX(SIZE(_QQ4_A,1),SIZE(_QQ4_B,1))-1)

#if _IS_ZERO !=1
    REAL(DP)::PP0(0:MAX(SIZE(_PP0_0,1),SIZE(_PP0_1,1))-1)
#else
    REAL(DP)::PP0(0:SIZE(_PP0_1,1)-1)
#endif
    REAL(DP)::PP1(0:SIZE(_PP1,1)-1)
    REAL(DP)::PP2(0:SIZE(_PP2,1)-1)

    INTEGER::QQ_WL(0:7)
    REAL(DP)::y,F(6),tau 
    PROCEDURE(CALC_TAIL_PROC),POINTER::CALC_TAIL
    INTEGER::ML,ML2,FL1,FL2
    INTEGER::I,J 
    REAL(DP)::C,P,FMUL
#if _IS_ZERO==1
    IF (l==0)   THEN
        RES=0.0_DP
        RETURN
    ENDIF
#endif
    ML=MIN(ABS(l),ABS(l-1))
    ML2=MAX(1,ABS(l))
    FL1=1
    FL2=1
    IF (l==0) FL1=3
    IF (l==1) FL2=3

    QQ_WL(1)=SIZE(QQ1)
    QQ_WL(2)=SIZE(_QQ2_B,1)
    QQ_WL(3)=SIZE(_QQ3_B,1)
    QQ_WL(4)=SIZE(_QQ4_B,1)
    QQ_WL(5)=SIZE(_PP0_1,1)
    IF (l<2) THEN
        CALC_TAIL=>CALC_TAIL_01
#if _IS_ZERO !=1
        IF (l==0)       THEN
            QQ_WL(5)=SIZE(_PP0_0,1)
            PP0(0:QQ_WL(5)-1)=_PP0_0
        ENDIF
#endif
        IF (l==1) PP0(0:SIZE(_PP0_1,1)-1)=_PP0_1
    ELSE
        CALC_TAIL=>CALC_TAIL_ZERO
    ENDIF

    IF (l<=LMAX(5)) THEN
        PP1=_PP1(:,l)
        PP2=_PP2(:,l)
    ENDIF

    IF (l<=LMAX(0)) THEN
        QQ0=_QQ0_COEFF(:,l)
        IF (l<=LMAX(1)) THEN
            QQ1=_QQ1_COEFF(:,l)
            IF (l<=2) THEN
                QQ_WL(2)=SIZE(_QQ2_A,1)
                QQ_WL(3)=SIZE(_QQ3_A,1)
                QQ_WL(4)=SIZE(_QQ4_A,1)

                QQ2(0:QQ_WL(2)-1)=_QQ2_A(:,l)
                QQ3(0:QQ_WL(3)-1)=_QQ3_A(:,l)
                QQ4(0:QQ_WL(4)-1)=_QQ4_A(:,l)
            ELSEIF (l<=LMAX(4)) THEN
                QQ2(0:QQ_WL(2)-1)=_QQ2_B(:,l)
                QQ3(0:QQ_WL(3)-1)=_QQ3_B(:,l)
                QQ4(0:QQ_WL(4)-1)=_QQ4_B(:,l)
            ELSEIF(l<=LMAX(3)) THEN
                QQ2(0:QQ_WL(2)-1)=_QQ2_B(:,l)
                QQ3(0:QQ_WL(3)-1)=_QQ3_B(:,l)
            ELSEIF(l<=LMAX(2)) THEN
                QQ2(0:QQ_WL(2)-1)=_QQ2_B(:,l)
            ENDIF
        ENDIF
    ELSE
        CALL COMPUTE_POL_COEFFICIENTS(l,_IS_EVEN,_N_COEFFS,_N_COEFFS_LEN,QQ0)
    ENDIF


    DO J = N1, N2 
        y = X(J) 
        P = 0.0_DP 
        C = 0.0_DP 
        F = MFUNCS(:,J) 
        IF (ABS(y*ML)>=LHMAX) THEN 
            RES(J:FILTER_END)=P 
            EXIT 
        ENDIF 
        IF (ABS(y) <=W_THRESHOLD(0)) THEN
            tau=ML2*y
            FMUL = CALC_FMUL(tau,_HPOW0,F(3))
            tau=tau*tau
#if MAX_HOR_ORDER > 5 
            CALL COMPUTE_POLYNOMIAL_HORNER(QQ0,SIZE(QQ0),tau,FMUL,P)
#else
            CALL COMPUTE_POLYNOMIAL_TAYLOR(QQ0,SIZE(QQ0),tau,FMUL,P)
#endif

#define _INTP 1
#include "compute_interval.F90" 
#undef _INTP
#define _INTP 2
#include "compute_interval.F90" 
#undef _INTP
#define _INTP 3
#include "compute_interval.F90" 
#undef _INTP
#define _INTP 4
#include "compute_interval.F90" 
#undef _INTP
        ELSE
            tau = y - W_THRESHOLD(4)
            CALL CALC_TAIL(PP0(0:QQ_WL(5)-1),tau,y,_HPOW,P)
            FMUL = CALC_FMUL(y,_TPOW,F(FL2))
            FMUL=_FRAC_MULT(l)*FMUL
            tau=1.0_DP/y/y
            CALL CALC_RAT_FUNC_MULT(PP1,PP2,tau,FMUL,C)
            P=P+C
        ENDIF
        RES(J) =  P 
    ENDDO
ENDSUBROUTINE

#undef _SUBNAME 
#undef _QQ0_COEFF 
#undef _QQ1_COEFF 

#undef _QQ2_A 
#undef _QQ3_A 
#undef _QQ4_A 

#undef _QQ2_B 
#undef _QQ3_B 
#undef _QQ4_B 

#undef _PP0_0 
#undef _PP0_1 

#undef _PP1 
#undef _PP2 

#undef _N_COEFFS 
#undef _N_COEFFS_LEN 

#undef _IS_EVEN 

#undef _HPOW0 
#undef _HPOW  
#undef _TPOW  
#undef _FRAC_MULT 
