        FUNCTION _FUNC (X) RESULT(RES)
                COMPLEX(DP),INTENT(IN)::X
                COMPLEX(DP)::RES(0:1)
                COMPLEX(DP)::y
                IF (ABS(X-_POINT0) <= _THRESHOLD0) THEN
                        y=X- _POINT0
                        RES(0)=COMPUTE_POLYNOMIAL(_ARR0_0,y)
                        RES(1)=COMPUTE_POLYNOMIAL(_ARR0_1,y)
                ELSEIF (ABS(x-_POINT1)<=_THRESHOLD1) THEN
                        y=X- _POINT1
                        RES(0)=COMPUTE_POLYNOMIAL(_ARR1_0,y)
                        RES(1)=COMPUTE_POLYNOMIAL(_ARR1_1,y)
                ELSEIF (ABS(x-_POINT2)<=_THRESHOLD2) THEN
                        y=X- _POINT2
                        RES(0)=COMPUTE_POLYNOMIAL(_ARR2_0,y)
                        RES(1)=COMPUTE_POLYNOMIAL(_ARR2_1,y)
                ELSE
                        y=_POINT3/X
                        RES(0)=COMPUTE_POLYNOMIAL(_ARR3_0,y)
                        RES(1)=COMPUTE_POLYNOMIAL(_ARR3_1,y)
                ENDIF
        ENDFUNCTION
