#include "CONCAT_MACRO.F90"
MODULE SUBROUTINES_FOR_DOUBLE_INTEGRATION_NP_14 
#if MAX_HOR_ORDER >= 14 
USE HANKEL_TRANSFORM_UTILITIES  
USE COEFFICIENTS_FOR_DOUBLE_INTEGRATION_NP_14
IMPLICIT NONE  
PRIVATE 
REAL(DP),PARAMETER::LHMAX=53.0_DP 
INTEGER,PARAMETER::LMAX(0:5)=[27,27,14,9,7,6]
REAL(DP),PARAMETER::W_THRESHOLD(0:4)=[2.0_DP,4.0_DP,6.0_DP,8.0_DP,10.0_DP] 
#include "Even_Defines_14.F90"
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_14_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_13_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_12_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_11_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_10_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_9_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_8_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_7_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_6_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_5_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_4_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_3_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_2_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_1_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_14_0_D2F4
CONTAINS
#define _FUNC _14_14_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_14_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_14_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_13_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_13_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_12_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_12_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_11_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_11_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_10_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_10_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_9_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_9_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_8_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_8_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_7_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_7_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_6_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_6_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_5_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_5_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_4_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_4_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_3_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_3_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_2_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_2_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_1_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_1_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _14_0_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_14_0_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#endif
ENDMODULE
