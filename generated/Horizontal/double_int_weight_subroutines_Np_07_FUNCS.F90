#include "CONCAT_MACRO.F90"
MODULE SUBROUTINES_FOR_DOUBLE_INTEGRATION_NP_7 
#if MAX_HOR_ORDER >= 7 
USE HANKEL_TRANSFORM_UTILITIES  
USE COEFFICIENTS_FOR_DOUBLE_INTEGRATION_NP_7
IMPLICIT NONE  
PRIVATE 
REAL(DP),PARAMETER::LHMAX=53.0_DP 
INTEGER,PARAMETER::LMAX(0:5)=[27,27,14,9,7,6]
REAL(DP),PARAMETER::W_THRESHOLD(0:4)=[2.0_DP,4.0_DP,6.0_DP,8.0_DP,10.0_DP] 
#include "Even_Defines_7.F90"
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_7_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_6_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_5_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_4_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_3_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_2_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_1_D2F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D1F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D1F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D1F4
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D2F0
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D2F2
PUBLIC ::  CALC_OUTPUT_FUNC_INT2_7_0_D2F4
CONTAINS
#define _FUNC _7_7_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_7_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_7_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_6_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_6_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_5_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_5_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_4_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_4_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_3_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_3_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_2_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_2_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_1_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_1_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D1F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D1F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D1F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D1F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D1F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D1F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D2F0
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D2F0
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D2F2
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D2F2
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#define _FUNC _7_0_D2F4
#define _IS_ZERO _IS_ZERO_AT_ZERO_7_0_D2F4
#include "double_integrate_template.F90"
#undef _FUNC
#undef _IS_ZERO
#endif
ENDMODULE
