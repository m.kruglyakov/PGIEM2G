


W_TAB_SRC = $(wildcard generated/Horizontal/*TABLES.F90)
W_TAB = $(patsubst %.F90,%.o,$(W_TAB_SRC))

W_SUBS_D_SRC = $(wildcard generated/Horizontal/double*FUNCS.F90)
W_SUBS_D = $(patsubst %.F90,%.o,$(W_SUBS_D_SRC))

W_COEFFS_D_SRC = $(wildcard generated/Horizontal/double*COEFFS.F90)
W_COEFFS_D = $(patsubst %.F90,%.o,$(W_COEFFS_D_SRC))

W_SUBS_S_SRC = $(wildcard generated/Horizontal/single*FUNCS.F90)
W_SUBS_S = $(patsubst %.F90,%.o,$(W_SUBS_S_SRC))

W_COEFFS_S_SRC = $(wildcard generated/Horizontal/single*COEFFS.F90)
W_COEFFS_S = $(patsubst %.F90,%.o,$(W_COEFFS_S_SRC))

LAT_WEIGHTS=$(W_TAB) $(W_SUBS_D) $(W_COEFFS_D)  $(W_SUBS_S)  $(W_COEFFS_S) 
