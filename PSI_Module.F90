MODULE PSI_MODULE
    USE CALCULATE_PSI_DIRECT_MODULE
    IMPLICIT NONE
    PRIVATE
    INTEGER, PARAMETER ::DP = SELECTED_REAL_KIND(15)
    REAL(DP),PARAMETER::INV_THRESHOLD=10
    INTERFACE CALCULATE_PSI
        MODULE PROCEDURE PSI_COMPLEX
    ENDINTERFACE
    PUBLIC:: CALCULATE_PSI

CONTAINS

    SUBROUTINE PSI_COMPLEX(Np,x,e,Psi) 
        INTEGER,INTENT(IN)::Np
        COMPLEX(DP),INTENT(IN)::x,e
        COMPLEX(DP),INTENT(INOUT)::Psi(2:Np)
        COMPLEX(DP)::P(0:1),y,P1(0:1)
        INTEGER::I,J,I1,Nm
        IF (Np<2) RETURN

        CALL CALCULATE_PSI_NMAX2(Np,X,e,P,Nm) 


        IF (ABS(x)<=INV_THRESHOLD ) THEN

            DO I=Nm-2,Np-1,-1
                J=MOD(I,2)
                P(J)=PSI_REC_X_COMPLEX(x,I,P(J))
            ENDDO
            J=MOD(Np,2)
            Psi(Np)=P(J)
            IF (Np==2) RETURN
            Psi(Np-1)=P(1-J)
            DO I=Np-2,2,-1
                Psi(I)=PSI_REC_X_COMPLEX(x,I,Psi(I+2))
            ENDDO
        ELSE
            y=1.0_DP/x
            DO I=Nm-2,Np-1,-1
                J=MOD(I,2)
                P(J)=PSI_REC_Y_COMPLEX(y,I,P(J))
            ENDDO
            J=MOD(Np,2)
            Psi(Np)=P(J)
            IF (Np==2) RETURN
            Psi(Np-1)=P(1-J)
            DO I=Np-2,2,-1
                Psi(I)=PSI_REC_Y_COMPLEX(y,I,Psi(I+2))
            ENDDO
        ENDIF
    ENDSUBROUTINE

    FUNCTION PSI_REC_X_COMPLEX(x,I,Psi) RESULT(RES)
        COMPLEX(DP),INTENT(IN)::x
        INTEGER,INTENT(IN)::I
        COMPLEX(DP),INTENT(IN)::Psi
        COMPLEX(DP)::RES
        RES = (-2.E+0_DP*I-3.E+0_DP)*x**2/&
            &(((2.E+0_DP*I-1.E+0_DP)*Psi-4.E+0_DP*I-2.E+0_DP)*x**2+&
            &I*(I*((-8.E+0_DP*I)-1.2E+1_DP)+2.E+0_DP)+3.E+0_DP)
    ENDFUNCTION

    FUNCTION PSI_REC_Y_COMPLEX(y,I,Psi) RESULT(RES)
        COMPLEX(DP),INTENT(IN)::y
        INTEGER,INTENT(IN)::I
        COMPLEX(DP),INTENT(IN)::Psi
        COMPLEX(DP)::RES
        RES = ((2.E+0_DP*I+3.E+0_DP))/&
            &((I*(I*(8.E+0_DP*I+1.2E+1_DP)-2.E+0_DP)-3.E+0_DP)*y**2+&
            &(1.E+0_DP-2.E+0_DP*I)*Psi+4.E+0_DP*I+2.E+0_DP)
    ENDFUNCTION

ENDMODULE
