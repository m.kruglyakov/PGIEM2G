#!/usr/bin/perl 

my $MODULE_NAME="CALCULATE_PSI_DIRECT_MODULE";
my $SUB_NAME="CALCULATE_PSI_";

my $ARR_NAME="PSI_COEFFS_";
my $POW_NAME="PSI_POW_";

my $NP_HLIM=128;

my @FT=("NUM","DENOM");
my @SS=("E","SE");

my @orders=(2 .. $ARGV[1]);

print STDERR "@orders $orders[1] \n";
my $NPMAX=$ARGV[2]+1;

push @orders, ($NPMAX-1, $NPMAX);

print "MODULE $MODULE_NAME \n";
print "\tUSE @ARGV[0] \n";
print "\tUSE CALCULATE_POLYNOMIAL_MODULE \n";
print "\tUSE SHIFTED_EXPONENT_MODULE \n";
print "IMPLICIT NONE \n";
print "PRIVATE \n\n";
print "\tINTEGER, PARAMETER ::DP = SELECTED_REAL_KIND(15) \n";
#print "\tINTEGER, PARAMETER ::NP_HARD_LIMIT=$NP_HLIM \n";
print "\tINTEGER, PARAMETER ::NP_MAX2= $NPMAX \n";
print "\tPUBLIC:: $SUB_NAME"."NMAX2, GET_NPMAX\n";
print "\tPUBLIC:: NP_MAX2 \n";
print "CONTAINS \n";
my $ident="\t";

print $ident,  "SUBROUTINE $SUB_NAME"."NMAX2(Np,X,E,PSI,Nm) \n";
$ident=$ident."\t";

print $ident,  "INTEGER,     INTENT(IN)::Np \n";
print $ident,  "COMPLEX(DP), INTENT(IN)::X,E \n";
print $ident,  "COMPLEX(DP), INTENT(OUT)::PSI(0:1) \n";
print $ident,  "INTEGER,     INTENT(OUT)::Nm \n";
print $ident,  "SELECT CASE (Np) \n";

$ident=$ident."\t";

print $ident, "CASE(2) \n";
print $ident, "\tCALL $SUB_NAME"."2(X,E,PSI(0))\n";
print $ident, "\tNm=Np\n";

my $I1;
my $I2;
foreach my $I (3 .. $ARGV[1]) {
    $I1=$I % 2;
    $I2=1-$I1;

    print $ident, "CASE($I) \n";
    print $ident, "\tCALL $SUB_NAME".($I-1)."(X,E,PSI($I2))\n";
    print $ident, "\tCALL $SUB_NAME".$I."(X,E,PSI($I1))\n";
    print $ident, "\tNm=Np\n";
} 
    $I1=$NPMAX % 2;
    $I2=1-$I1;
    
    
    print $ident, "CASE( ". ($ARGV[1]+1). ":" .($NPMAX)." ) \n";
    print $ident, "\tCALL $SUB_NAME".($NPMAX-1)."(X,E,PSI($I2))\n";
    print $ident, "\tCALL $SUB_NAME".($NPMAX)."(X,E,PSI($I1))\n";
    print $ident, "\tNm=".($NPMAX)."\n";

    #    print $ident, "CASE( ". ($NPMAX+1). ":" .($NP_HLIM)." ) \n";
    #    print $ident, "\t A=0.0_DP; B=0.0_DP; C=0.0_DP\n";
    #    print $ident, "\tNm=".($NP_HLIM)."\n";

print $ident, "CASE DEFAULT \n  $ident \t STOP 10 \n";
chop $ident;
print $ident, "END SELECT\n";
chop $ident;
print $ident, "ENDSUBROUTINE\n\n";

print $ident,  "FUNCTION GET_NPMAX"."(Np) RESULT(RES) \n";
$ident=$ident."\t";

print $ident,  "INTEGER,     INTENT(IN)::Np \n";
print $ident,  "INTEGER     RES \n";
print $ident,  "RES=MIN(Np,$NPMAX) \n";

chop $ident;
print $ident, "ENDFUNCTION\n\n";

print "!!!!!!!!      PRIVATE    \n\n";
my $aname;
my $pname;

foreach $I (@orders){
    print $ident, "SUBROUTINE $SUB_NAME$I(X,E,PSI)\n";
    $ident=$ident."\t";
    print $ident,  "COMPLEX(DP),INTENT(IN)::X,E \n";
    print $ident,  "COMPLEX(DP),INTENT(OUT)::PSI \n";
    print $ident,  "COMPLEX(DP)::y,se \n";
    print $ident,  "COMPLEX(DP)::p0,p1,q0,q1,A,B \n";
    #    print $ident,  "e=EXP(-2.0_DP*X) \n";
    print $ident,  "IF (ABS(X) < ".(($I+1)/2.0).") THEN \n";
    $ident=$ident."\t";
    print $ident,  "se=SHIFTED_EXP(-2.0_DP*X,e,".($I)."*N_NORM)\n";
        #   PSI_COEFFS_NUM_SE_2_0
    $aname=$ARR_NAME.$FT[0]."_$SS[1]_$I";
    $pname=$POW_NAME.$FT[0]."_$SS[1]_$I";
    print $ident,   "p0=COMPUTE_POLYNOMIAL(", $aname."_0, x) \n";
    print $ident,   "p1=COMPUTE_POLYNOMIAL(", $aname."_1, x) \n";
    print $ident,   "A=x**$pname"."_0*p0+x**$pname"."_1*p1*se \n\n";

    $aname=$ARR_NAME.$FT[1]."_$SS[1]_$I";
    $pname=$POW_NAME.$FT[1]."_$SS[1]_$I";
    print $ident,   "q0=COMPUTE_POLYNOMIAL(", $aname."_0, x) \n";
    print $ident,   "q1=COMPUTE_POLYNOMIAL(", $aname."_1, x) \n";
    print $ident,   "B=x**$pname"."_0*q0+x**$pname"."_1*q1*se \n";
    print $ident,   "PSI=A/B \n\n";
    
    chop $ident;
    print $ident,  "ELSE\n";
    $ident=$ident."\t";
    print $ident,  "y=1.0_DP/x\n";
    $aname=$ARR_NAME.$FT[0]."_$SS[0]_$I";
    $pname=$POW_NAME.$FT[0]."_$SS[0]_$I";
    print $ident,   "p0=COMPUTE_POLYNOMIAL(", $aname."_0, y) \n";
    print $ident,   "p1=COMPUTE_POLYNOMIAL(", $aname."_1, y) \n";
    print $ident,   "A=x**$pname"."_0*p0+x**$pname"."_1*p1*e \n\n";

    $aname=$ARR_NAME.$FT[1]."_$SS[0]_$I";
    $pname=$POW_NAME.$FT[1]."_$SS[0]_$I";
    print $ident,   "q0=COMPUTE_POLYNOMIAL(", $aname."_0, y) \n";
    print $ident,   "q1=COMPUTE_POLYNOMIAL(", $aname."_1, y) \n";
    print $ident,   "B=x**$pname"."_0*q0+x**$pname"."_1*q1*e \n";
    print $ident,   "PSI=A/B \n\n";
    
    chop $ident;
    print $ident, "ENDIF\n";
    chop $ident;
    print $ident, "ENDSUBROUTINE \n";
}
print "END MODULE";
