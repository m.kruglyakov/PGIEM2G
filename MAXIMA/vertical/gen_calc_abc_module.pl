#!/usr/bin/perl 

my $MODULE_NAME="CALCULATE_EXP_ABS_DIRECT_MODULE";
my $SUB_NAME="CALCULATE_ABC_";

my $ARR_NAME="SINGL_";
my $POW_NAME="SINGL_POW_";

my $NP_HLIM=128;

my @FT=("A","B","C");
my @orders=(0 .. $ARGV[1]);

print STDERR "@orders $orders[1] \n";
my $NPMAX=$ARGV[2]+1;

push @orders, ($NPMAX-1, $NPMAX);

print "MODULE $MODULE_NAME \n";
print "\tUSE @ARGV[0] \n";
print "\tUSE CALCULATE_POLYNOMIAL_MODULE \n";
print "\tUSE SHIFTED_EXPONENT_MODULE \n";
print "IMPLICIT NONE \n";
print "PRIVATE \n\n";
print "\tINTEGER, PARAMETER ::DP = SELECTED_REAL_KIND(15) \n";
print "\tINTEGER, PARAMETER ::NP_HARD_LIMIT=$NP_HLIM \n";
print "\tINTEGER, PARAMETER ::NP_MAX= $NPMAX \n";
print "\tPUBLIC:: $SUB_NAME"."NMAX, GET_NPMAX\n";
print "\tPUBLIC:: NP_MAX, NP_HARD_LIMIT \n";
print "CONTAINS \n";
my $ident="\t";

print $ident,  "SUBROUTINE $SUB_NAME"."NMAX(Np,X,E,A,B,C,Nm) \n";
$ident=$ident."\t";

print $ident,  "INTEGER,     INTENT(IN)::Np \n";
print $ident,  "COMPLEX(DP), INTENT(IN)::X,E \n";
print $ident,  "COMPLEX(DP), INTENT(OUT)::A(0:1), B(0:1), C(0:1) \n";
print $ident,  "INTEGER,     INTENT(OUT)::Nm \n";
print $ident,  "SELECT CASE (Np) \n";

$ident=$ident."\t";

print $ident, "CASE(0) \n";
print $ident, "\tCALL $SUB_NAME"."0(X,E,A(0),B(0),C(0))\n";
print $ident, "\tNm=Np\n";

my $I1;
my $I2;
foreach my $I (1 .. $ARGV[1]) {
    $I1=$I % 2;
    $I2=1-$I1;

    print $ident, "CASE($I) \n";
    print $ident, "\tCALL $SUB_NAME".($I-1)."(X,E,A($I2),B($I2),C($I2))\n";
    print $ident, "\tCALL $SUB_NAME".$I."(X,E,A($I1),B($I1),C($I1))\n";
    print $ident, "\tNm=Np\n";
} 
    $I1=$NPMAX % 2;
    $I2=1-$I1;
    
    
    print $ident, "CASE( ". ($ARGV[1]+1). ":" .($NPMAX)." ) \n";
    print $ident, "\tCALL $SUB_NAME".($NPMAX-1)."(X,E,A($I2),B($I2),C($I2))\n";
    print $ident, "\tCALL $SUB_NAME".($NPMAX)."(X,E,A($I1),B($I1),C($I1))\n";
    print $ident, "\tNm=".($NPMAX)."\n";

    print $ident, "CASE( ". ($NPMAX+1). ":" .($NP_HLIM)." ) \n";
    print $ident, "\t A=0.0_DP; B=0.0_DP; C=0.0_DP\n";
    print $ident, "\tNm=".($NP_HLIM)."\n";

print $ident, "CASE DEFAULT \n  $ident \t STOP 10 \n";
chop $ident;
print $ident, "END SELECT\n";
chop $ident;
print $ident, "ENDSUBROUTINE\n\n";

print $ident,  "FUNCTION GET_NPMAX"."(Np) RESULT(RES) \n";
$ident=$ident."\t";

print $ident,  "INTEGER,     INTENT(IN)::Np \n";
print $ident,  "INTEGER     RES \n";
print $ident,  "RES=MIN(Np,$NPMAX,$NP_HLIM) \n";

chop $ident;
print $ident, "ENDFUNCTION\n\n";

print "!!!!!!!!      PRIVATE    \n\n";

foreach $I (@orders){
    print $ident, "SUBROUTINE $SUB_NAME$I(X,E,A,B,C)\n";
    $ident=$ident."\t";
    print $ident,  "COMPLEX(DP),INTENT(IN)::X,E \n";
    print $ident,  "COMPLEX(DP),INTENT(OUT)::A, B, C \n";
    print $ident,  "COMPLEX(DP)::y,se \n";
    print $ident,  "COMPLEX(DP)::p0,p1 \n";
    #    print $ident,  "e=EXP(-2.0_DP*X) \n";
    print $ident,  "IF (ABS(X) < ".(($I+1)/2.0).") THEN \n";
    $ident=$ident."\t";
    print $ident,  "se=SHIFTED_EXP(-2.0_DP*X,e,".($I+1)."*N_NORM)\n";
    foreach $f (@FT){
        my $aname=$ARR_NAME.$f."_".$I;
        my $pname=$POW_NAME.$f."_".$I;
        print $ident,   "p0=COMPUTE_POLYNOMIAL(", $aname."_3, x) \n";
        print $ident,   "p1=COMPUTE_POLYNOMIAL(", $aname."_4, x) \n";
        print $ident,   "$f=x**$pname"."_3*p0+x**$pname"."_4*p1*se \n\n";

    }
    chop $ident;
    print $ident,  "ELSE\n";
    $ident=$ident."\t";
    print $ident,  "y=1.0_DP/x\n";
    foreach $f (@FT){
        my $aname=$ARR_NAME.$f."_".$I;
        my $pname=$POW_NAME.$f."_".$I;
        print $ident,   "p0=COMPUTE_POLYNOMIAL(", $aname."_1, y) \n";
        print $ident,   "p1=COMPUTE_POLYNOMIAL(", $aname."_2, y) \n";
        print $ident,   "$f=y**$pname"."_1*p0+y**$pname"."_2*p1*e \n\n";
    }
    chop $ident;
    print $ident, "ENDIF\n";
    chop $ident;
    print $ident, "ENDSUBROUTINE \n";
}
print "END MODULE";
