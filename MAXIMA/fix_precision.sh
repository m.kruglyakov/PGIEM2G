#!/bin/sh

sed -i 's/ _dp/_DP/g' $1
sed -i -z 's/ \&\n\&_dp/_DP\&\n\&/g' $1
sed -i -z 's/\&\n\&_dp/_DP\&\n\&/g' $1
sed -i -z 's/ _\&\n\&dp/_DP\&\n\&/g' $1
sed -i -z 's/ _d\&\n\&p/_DP\&\n\&/g' $1
sed -i 's/d+/E+/g' $1
sed -i 's/d-/E-/g' $1

sed -i -z 's/d\&\n\&/E\&\n\&/g' $1
sed -i -z 's/\&\n\&\n/\n/g' $1
sed -i 's/COLON/:/g' $1
sed -i 's/\t /\t/g' $1
