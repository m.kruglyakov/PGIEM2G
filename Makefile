
ifdef LOCAL_OPTIONS
	include $(LOCAL_OPTIONS)
	PLATFORM:=$(LOCAL_OPTIONS)
else
ifndef PLATFORM
	PLATFORM=GNU
endif
LOCAL_OPTIONS=make.inc
endif




ifneq ($(MAKECMDGOALS), clean)
FFT_QUAD_OOURA="NO"
ifndef LOCAL_OPTIONS
include make.inc	
endif
else
FFT_QUAD_OOURA="YES"
endif

ifdef TEST
	REBUILD:=1
endif

ifeq ($(LONGTRANSFORM),YES)
FOPTS+=-D LONGTRANSFORM
ifdef INSTALL_PATH
INSTALL_PATH:=$(INSTALL_PATH)_long
endif
endif
ifeq ($(QUADWEIGHTS),YES)
FOPTS+=-D QUADWEIGHTS
ifdef INSTALL_PATH
INSTALL_PATH:=$(INSTALL_PATH)_quad
endif
endif

OPTS=$(FOPTS)

ifeq ($(FFT_QUAD_OOURA),"YES")
	OPTS=$(FOPTS) -D FFT_QUAD_OOURA
endif



STATIC_INC=-I$(FFTW_INC) $(BLAS_INC) -I./GFGMRES -I./FSON -I./generated/Vertical -I./generated/Horizontal  -I./generated/include  -I./generated/Templates

ifndef EXTERNAL_SRC
GEN_UTILS=General_Utilities
FSON=FSON
GFGMRES=GFGMRES
else
GEN_UTILS=$(EXTERNAL_SRC)/General_Utilities
FSON=$(EXTERNAL_SRC)/FSON
GFGMRES=$(EXTERNAL_SRC)/GFGMRES
STATIC_INC+=-I$(EXTERNAL_SRC)
endif

LIBS=  $(LIB_ADD)  -L$(LIB_FFTW) $(LIB_FGMRES) -L$(LIB_BLAS)  $(LIB_FSON) 


OPTS+=$(USER_OPTIONS)


ALL_O=$(shell cat all_o)

PGIEM2G_KERNEL_O=$(shell cat pgiem2g_kernel_o)

ifdef MAX_HOR_ORDER
	TMP:=$(shell perl -pi -e  's/MAX_HOR_ORDER=(\d+)/MAX_HOR_ORDER=$(MAX_HOR_ORDER)/g' order_ctl.inc) 
endif


ifneq ($(PLATFORM), BGP_FAST)
OPTS+=-D RECIEVERS_AT_CELL_BORDERS
OPTS+=-D USE_OLD_LATERAL_PWC
include order_ctl.inc
else
OPTS+=-WF,-DRECIEVERS_AT_CELL_BORDERS
FOPTS+=-WF,-DMAX_HOR_ORDER=2
OPTS+=-WF,-DUSE_OLD_LATERAL_PWC 
endif



giem2g:  $(ALL_O) giem2g.F90	Makefile $(LOCAL_OPTIONS)
	$(FC_Link)   $(OPTS) $(LOPTS)  giem2g.F90   $(ALL_O)  $(LIBS)  $(STATIC_INC) -o giem2g  $(DYN)

	
ifdef INSTALL_PATH
	cp giem2g $(INSTALL_PATH)
endif


giem2g_svd: libgiem2g.a giem2g_svd.F90 TRY_SVD_MODULE.F90 
	$(FC_Link)   $(OPTS) TRY_SVD_MODULE.F90  giem2g_svd.F90   -L./ -lgiem2g  $(LIBS)  $(STATIC_INC) -o giem2g_svd  $(DYN)


libgiem2g.a:   $(PGIEM2G_KERNEL_O) Makefile $(LOCAL_OPTIONS)
	@echo $(EXTERNAL_SRC)
ifdef PLATFORM
	@echo "PLATFORM: " $(PLATFORM)
else
	@echo "LOCAL MAKE FILE: " $(LOCAL_OPTIONS)	
endif
	$(AR) rcs libgiem2g.a $(PGIEM2G_KERNEL_O) 

ifdef REBUILD
$(ALL_O): clean
endif

$(ALL_O): order_ctl.inc build_deps all_o
$(PGIEM2G_KERNEL_O): Makefile $(LOCAL_OPTIONS)

include build_deps

Volume_Hankel_TransformsP_Module.F90:  generated/include/WEIGHT_SUBROUTINES_LIST.F90 order_ctl.inc 
Hankel_Transform_Utilities.F90:  order_ctl.inc


%.o:%.f90 Makefile $(LOCAL_OPTIONS)

	$(FC) $(OPTS) -c $*.f90 -o $*.o $(STATIC_INC) 

%.o:%.F90 Makefile $(LOCAL_OPTIONS)

ifneq ($(SHARED_LIB),1)
	$(FC) $(OPTS) -c $*.F90 -o $*.o $(STATIC_INC) 

else
	$(FC) $(OPTS) -fPIC -D ExtrEMe   -c $*.F90 -o $*.o -I$(SHARED_BLAS_INC)  -I$(SHARED_FFTW_INC) 
endif


clean:
	-rm -f $(ALL_O) *.mod libgiem2g.a
	@echo "CLEANED"

