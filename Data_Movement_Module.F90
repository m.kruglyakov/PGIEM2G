!Copyright (c) 2017 Mikhail Kruglyakov 
!This file is part of PGIEM2G.
!
!PGIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE	DATA_MOVING_MODULE 
	USE CONST_MODULE
	USE MPI_MODULE

	IMPLICIT NONE
	INTEGER(MPI_CTL_KIND),PARAMETER::MPI_TWO=2
	INTEGER(MPI_CTL_KIND),PARAMETER::MPI_ONE=1
	PRIVATE:: MPI_TWO,MPI_ONE
CONTAINS
#ifdef use_this_module
	SUBROUTINE MOVE_ELECTRICAL_FIELD(Ein,comm_in,comm_out,Eout)
			COMPLEX(REALPARM),INTENT(IN),POINTER::Ein(:,:,:,:,:,:,:)
			INTEGER(MPI_CTL_KIND),INTENT(IN)::comm_in,comm_out
			COMPLEX(REALPARM),INTENT(IN),POINTER::Eout(:,:,:,:,:,:,:)
			INTEGER(MPI_CTL_KIND)::me,me_in,me_out
			INTEGER(MPI_CTL_KIND)::me_out2,me_in2,me_in3
			INTEGER(MPI_CTL_KIND)::size_in,size_out
			INTEGER(MPI_CTL_KIND)::comm,comm_in2,comm_out2
			INTEGER(MPI_CTL_KIND)::comm2
			INTEGER(MPI_CTL_KIND)::psend,precv
			INTEGER(MPI_CTL_KIND)::reqs,reqr
			INTEGER(MPI_CTL_KIND)::IERROR
			INTEGER(MPI_CTL_KIND)::st(MPI_STATUS_SIZE)
			INTEGER(MPI_CTL_KIND),ALLOCATABLE::pos_in(:)
			INTEGER(MPI_CTL_KIND),ALLOCATABLE::pos_out(:)

			INTEGER::Ny_loc_in,Ny_loc_out
			INTEGER::N,M
			INTEGER::shape_in(7),shape_out(7)
			COMPLEX(REALPARM),POINTER::Etmp(:,:,:,:,:,:,:)
			IF ((comm_in/=MPI_COMM_NULL).AND.(ASSOCIATED(Ein))) THEN
				CALL MPI_COMM_SIZE(comm_in,size_in,IERROR) 
				CALL MPI_COMM_RANK(comm_in,me_in,IERROR)
				Ny_loc_in=SIZE(Ein,6)
			ELSE
				me_in=-1
				size_in=-1
				Ny_loc_in=-1
			ENDIF
			IF ((comm_out/=MPI_COMM_NULL).AND.(ASSOCIATED(Eout))) THEN
				CALL MPI_COMM_SIZE(comm_out,size_out,IERROR) 
				CALL MPI_COMM_RANK(comm_out,me_out,IERROR)
				Ny_loc_out=SIZE(Eout,6)
			ELSE
				me_out=-1
				size_out=-1
				Ny_loc_out=-1
			ENDIF
			CALL MPI_COMM_RANK(MPI_COMM_WORLD,me,IERROR) 
			IF ((me_in>=0) .OR. (me_out>=0) ) THEN
				CALL MPI_COMM_SPLIT(MPI_COMM_WORLD,MPI_ONE, me, comm, IERROR)
			ELSE
				CALL MPI_COMM_SPLIT(MPI_COMM_WORLD,MPI_TWO, me, comm, IERROR)
				CALL MPI_COMM_FREE(comm,IERROR)
				RETURN
			ENDIF
			comm2=CONNECT_ROOTS(me_in,me_out,comm)
			IF (me_in==0) THEN
				IF (me_out==0) THEN
					shape_out=SHAPE(Eout)
				ELSE
					CALL MPI_RECV(shape_out, 7, MPI_INTEGER, 1, 0,comm2,st,IERROR)
					CALL MPI_COMM_FREE(comm2,IERROR)
				ENDIF
			ELSEIF(me_out==0) THEN
				shape_out=SHAPE(Eout)
				CALL MPI_SEND(shape_out, 7, MPI_INTEGER, 0, 0, comm2, IERROR)
				CALL MPI_COMM_FREE(comm2,IERROR)
			ENDIF
			IF (me_in>=0) THEN
				CALL MPI_BCAST(shape_out,7,MPI_INTEGER,0,comm_in, IERROR)
				shape_in=SHAPE(Ein)
				IF (ALL(shape_out==shape_in)) THEN
					Etmp=>Ein
					CALL MPI_COMM_DUP(comm_in,comm_in2,IERROR)
				ELSEIF (ALL(shape_out(1:5)==shape_in(1:5)) .AND.&
							& shape_out(6)>shape_in(6) .AND. shape_in(7)==shape_out(7) ) THEN
					Ny_loc_out=shape_out(6)
					CALL GATHER_ELECTRICAL_FIELD(Ein,comm_in,Ny_loc_out,comm_in2,Etmp)
				ELSEIF (ALL(shape_out(1:5)==shape_in(1:5)) .AND. &
							& shape_out(6)<shape_in(6) .AND. shape_in(7)==shape_out(7) ) THEN
				ELSE
				ENDIF
				IF (comm_in2/=MPI_COMM_NULL) THEN
					CALL MPI_COMM_RANK(comm_in2,me_in2,IERROR)
					CALL MPI_COMM_SIZE(comm_in2,size_in,IERROR)
				ELSE
					me_in2=-1
				ENDIF
			ELSE
				me_in2=-1
			ENDIF
			IF (me_in2>=0) THEN
				IF (me_in2==0)	ALLOCATE(pos_in(size_in))
				CALL MPI_COMM_RANK(comm,me_in3,IERROR)
				CALL MPI_GATHER(me_in3, MPI_ONE, MPI_INTEGER, pos_in, MPI_ONE,&
					 &  MPI_INTEGER, 0, comm_in2,  IERROR)
			ENDIF
			IF (me_out>=0) THEN
				CALL MPI_COMM_RANK(comm,me_out2,IERROR)
				IF (me_out==0)	ALLOCATE(pos_out(size_out))
				CALL MPI_GATHER(me_out2, MPI_ONE, MPI_INTEGER, pos_out, MPI_ONE,&
					 &  MPI_INTEGER, 0, comm_out,  IERROR)
			ENDIF

			comm2=CONNECT_ROOTS(me_in2,me_out,comm)

			IF (me_in2==0) THEN
				IF (me_out==0) THEN
					CALL MPI_COMM_FREE(comm2,IERROR)
				ELSE
					ALLOCATE(pos_out(size_in))
					CALL MPI_RECV(pos_out, size_in, MPI_INTEGER, 1, 1,comm2,st,IERROR)
					CALL MPI_SEND(pos_in, size_in, MPI_INTEGER, 1, 2, comm2, IERROR)
					CALL MPI_COMM_FREE(comm2,IERROR)
				ENDIF
			ELSEIF(me_out==0) THEN
				ALLOCATE(pos_in(size_out))
				CALL MPI_SEND(pos_out, size_out, MPI_INTEGER, 0, 1, comm2, IERROR)
				CALL MPI_RECV(pos_in, size_out, MPI_INTEGER, 0, 2,comm2,st,IERROR)
				CALL MPI_COMM_FREE(comm2,IERROR)
			ENDIF
			IF (me_in2>=0) THEN
				CALL MPI_SCATTER(pos_out, MPI_ONE,MPI_INTEGER,&
						&precv,MPI_ONE,MPI_INTEGER,0,comm_in2,IERROR)
			ENDIF
			IF (me_out>=0) THEN
				CALL MPI_SCATTER(pos_in, MPI_ONE,MPI_INTEGER,&
						&psend,MPI_ONE,MPI_INTEGER,0,comm_out,IERROR)
			ENDIF

			IF (me_in2>=0) THEN
				CALL MPI_ISEND(Etmp, SIZE(Etmp), MPI_DOUBLE_COMPLEX, precv, me_in2+3, comm,reqs, IERROR)
			ENDIF

			IF (me_out>=0) THEN
				CALL MPI_IRECV(Eout, SIZE(Eout), MPI_DOUBLE_COMPLEX, psend, me_out+3, comm,reqr, IERROR)
			ENDIF
		

			IF (me_in2>=0) THEN
				CALL MPI_WAIT(reqs,st, IERROR)		
				CALL MPI_COMM_FREE(comm_in2,IERROR)
				IF (.NOT. ASSOCIATED(Etmp,Ein)) DEALLOCATE(Etmp)
			ENDIF
			IF (me_out>=0) THEN
				CALL MPI_WAIT(reqr,st, IERROR)
			ENDIF
			CALL MPI_COMM_FREE(comm,IERROR)
			IF (ALLOCATED(pos_in)) DEALLOCATE(pos_in)
			IF (ALLOCATED(pos_out)) DEALLOCATE(pos_out)
	ENDSUBROUTINE

	SUBROUTINE	GATHER_ELECTRICAL_FIELD(Ein,comm_in,Ny_loc_out,comm_out,Eout,Ny_offset_out)
			COMPLEX(REALPARM),INTENT(IN)::Ein(1:,0:,0:,0:,1:,1:,1:)
			INTEGER(MPI_CTL_KIND),INTENT(IN)::comm_in
			INTEGER,INTENT(IN)::Ny_loc_out
			INTEGER(MPI_CTL_KIND),INTENT(OUT)::comm_out
			COMPLEX(REALPARM),INTENT(OUT),POINTER::Eout(:,:,:,:,:,:,:)
			INTEGER,INTENT(OUT),OPTIONAL::Ny_offset_out
			COMPLEX(REALPARM),POINTER::Etmp(:,:,:)
			INTEGER::Ny,Ny_loc_in
			INTEGER::Ny_offset_in
			INTEGER(MPI_CTL_KIND)::Np_in,IERROR
			INTEGER(MPI_CTL_KIND)::me_in,me_out,me
			INTEGER(MPI_CTL_KIND)::color_send,color_recv
			INTEGER(MPI_CTL_KIND)::comm_gather,comm_recv
			INTEGER::N,M,Np
			INTEGER::Iz,Nz,Ip,l
			CALL MPI_COMM_SIZE(comm_in,Np_in,IERROR) 
			CALL MPI_COMM_RANK(comm_in,me_in,IERROR)
			Nz=SIZE(Ein,7)
			Ny=Np_in*SIZE(Ein,6)
			Ny_loc_in=SIZE(Ein,6)
			Ny_offset_in=me_in*Ny_loc_in
			N=SIZE(Ein)
			color_send=Ny_offset_in/Ny_loc_out
			CALL MPI_COMM_SPLIT(comm_in, color_send, Ny_offset_in, comm_gather, IERROR)

			CALL MPI_COMM_RANK(comm_gather,me,IERROR)
			CALL MPI_COMM_SIZE(comm_gather,Np,IERROR) 
			IF (me==0) THEN
				M=SIZE(Ein)/Nz
				ALLOCATE(Etmp(M,Nz,Np))
				ALLOCATE(Eout(1:SIZE(Ein,1),0:SIZE(Ein,2)-1,0:SIZE(Ein,3)-1,&
					   &0:SIZE(Ein,4)-1,1:SIZE(Ein,5),1:Ny_loc_out,&
					   &1:Nz))

			ELSE
				Etmp=>NULL()
				Eout=>NULL()
			ENDIF
			CALL MPI_GATHER(Ein, N, MPI_DOUBLE_COMPLEX, Etmp, N,&
					 &  MPI_DOUBLE_COMPLEX, 0, comm_gather,  IERROR)
			 IF (me==0) THEN
				DO Iz=1,Nz
					l=1
					DO Ip=1,Np
						CALL ZCOPY(M,Etmp(:,Iz,Ip),ONE,Eout(:,:,:,:,:,l:,Iz),ONE)  
						l=l+Ny_loc_in
					ENDDO
				ENDDO
				DEALLOCATE(Etmp)
			ENDIF
			IF (me==0) THEN
				CALL MPI_COMM_SPLIT(comm_in,MPI_ONE, Ny_offset_in, comm_out, IERROR)
				IF (PRESENT(Ny_offset_out)) THEN
					CALL MPI_COMM_RANK(comm_out,me_out,IERROR)
					Ny_offset_out=me_out*Ny_loc_out
				ENDIF
			ELSE
				CALL MPI_COMM_SPLIT(comm_in, MPI_TWO, Ny_offset_in, comm_out, IERROR)
				CALL MPI_COMM_FREE(comm_out,IERROR)
				comm_out=MPI_COMM_NULL
				IF (PRESENT(Ny_offset_out)) Ny_offset_out=-1
			ENDIF
			CALL MPI_COMM_FREE(comm_gather,IERROR)
	ENDSUBROUTINE
	SUBROUTINE	SCATTER_ELECTRICAL_FIELD(Ein,comm_in,Ny_loc_out,comm_pool,comm_out,Eout,Ny_offset_out)
			COMPLEX(REALPARM),POINTER,INTENT(IN)::Ein(:,:,:,:,:,:,:)
			INTEGER(MPI_CTL_KIND),INTENT(IN)::comm_in,comm_pool
			INTEGER,INTENT(IN)::Ny_loc_out
			INTEGER(MPI_CTL_KIND),INTENT(OUT)::comm_out
			COMPLEX(REALPARM),INTENT(OUT),POINTER::Eout(:,:,:,:,:,:,:)
			INTEGER,INTENT(OUT),OPTIONAL::Ny_offset_out
			COMPLEX(REALPARM),POINTER::Etmp(:,:,:)
			INTEGER::Ny,Ny_loc_in
			INTEGER::Ny_offset_in
			INTEGER(MPI_CTL_KIND)::Np_in,IERROR
			INTEGER(MPI_CTL_KIND)::me_in,me_out,me,me2,me_out2
			INTEGER(MPI_CTL_KIND)::comm2
			INTEGER(MPI_CTL_KIND)::color
			INTEGER(MPI_CTL_KIND)::comm_scatter
			INTEGER(MPI_CTL_KIND)::st(MPI_STATUS_SIZE)
			INTEGER(MPI_CTL_KIND)::Np(3)
			INTEGER::N,M,Nscale,K
			INTEGER::Iz,Nz,Ip,l
			IF ((comm_in/=MPI_COMM_NULL).AND. ASSOCIATED(Ein))THEN
				CALL MPI_COMM_SIZE(comm_in,Np_in,IERROR) 
				CALL MPI_COMM_RANK(comm_in,me_in,IERROR)
				Np(1)=Np_in
				Np(2)=(Np_in*SIZE(Ein,6))/Ny_loc_out
				Np(3)=SIZE(Ein,6)
			ELSE
				me_in=-1
			ENDIF	

			CALL MPI_COMM_RANK(comm_pool,me,IERROR)
			comm2=CONNECT_ROOTS(me_in,me,comm_pool)

			IF (me_in==0) THEN
				IF (me==0) THEN
					CALL MPI_COMM_FREE(comm2,IERROR)
				ELSE
					CALL MPI_SEND(Np, 2, MPI_INTEGER, 2, 0, comm2, IERROR)
					CALL MPI_COMM_FREE(comm2,IERROR)
				ENDIF
			ELSEIF(me==0) THEN
				CALL MPI_RECV(Np_in, 2, MPI_INTEGER, 0, 0,comm2,st,IERROR)
				CALL MPI_COMM_FREE(comm2,IERROR)
			ENDIF
			CALL MPI_BCAST(Np,3,MPI_INTEGER,0,comm_pool, IERROR)
			Np_in=Np(1)
			Nscale=Np(2)/Np(1)
			IF (me_in>=0) THEN
				CALL MPI_COMM_SPLIT(comm_pool, MPI_ONE, me_in, comm2, IERROR)
			ELSE
				CALL MPI_COMM_SPLIT(comm_pool, MPI_ONE, Np_in+1, comm2, IERROR)
			ENDIF
			CALL MPI_COMM_RANK(comm2,me2,IERROR)
			CALL MPI_COMM_FREE(comm2,IERROR)
			IF  (me2 < Np(2) ) THEN
				CALL MPI_COMM_SPLIT(comm_pool, MPI_ONE, me2, comm2, IERROR)
			ELSE
				CALL MPI_COMM_SPLIT(comm_pool, MPI_TWO, me2, comm2, IERROR)
				CALL MPI_COMM_FREE(comm2,IERROR)
				comm_out=MPI_COMM_NULL
				Eout=>NULL()
				RETURN
			ENDIF
			IF (me_in>=0) THEN
				Ny_offset_in=me_in*SIZE(Ein,6)
				CALL MPI_COMM_SPLIT(comm2,MPI_ONE, Ny_offset_in,  comm_out, IERROR)
				color=Ny_offset_in/Np(3)
			ELSE
				CALL MPI_COMM_RANK(comm2,me_out2,IERROR)
				K=(Np(3)-1)/Ny_loc_out
				Ny_offset_out=(me_out2/K)*Nscale+MODULO(me_out2,K)+1
				CALL MPI_COMM_SPLIT(comm2, MPI_ONE, Ny_offset_out,  comm_out, IERROR)
				color=Ny_offset_out/Np(3)
			ENDIF
			CALL MPI_COMM_FREE(comm2,IERROR)
			CALL MPI_COMM_RANK(comm_out,me_out,IERROR)

			CALL MPI_COMM_SPLIT(comm_out,color,me_out,  comm_scatter, IERROR)

			CALL MPI_COMM_FREE(comm_scatter,IERROR)
	ENDSUBROUTINE

	FUNCTION CONNECT_ROOTS(me1,me2,comm) RESULT(comm_res)
			INTEGER(MPI_CTL_KIND),INTENT(IN)::me1,me2,comm
			INTEGER(MPI_CTL_KIND)::comm_res
			INTEGER(MPI_CTL_KIND)::IERROR
			IF (me1==0) THEN
				CALL MPI_COMM_SPLIT(comm,MPI_ONE, -MPI_TWO, comm_res, IERROR)
			ELSEIF(me2==0) THEN
				CALL MPI_COMM_SPLIT(comm,MPI_ONE, -MPI_ONE, comm_res, IERROR)
			ELSE
				CALL MPI_COMM_SPLIT(comm,MPI_TWO, MPI_ONE, comm_res, IERROR)
				CALL MPI_COMM_FREE(comm_res,IERROR)
				comm_res=MPI_COMM_NULL
			ENDIF
	ENDFUNCTION
#endif
ENDMODULE
