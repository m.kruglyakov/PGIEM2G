!Copyright (c) 2017 Mikhail Kruglyakov 
!This file is part of PGIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE  FIELDS_AND_CURRENTS_MODULE 
    USE CONST_MODULE
    USE DATA_TYPES_MODULE
    USE OPERATORS_MODULE 
    IMPLICIT NONE

    INTERFACE ALLOCATE_E
        MODULE PROCEDURE ALLOCATE_E_IE
        MODULE PROCEDURE ALLOCATE_E_RC
    ENDINTERFACE

    INTERFACE FIELD_TO_CURRENT
        MODULE PROCEDURE FIELD_TO_CURRENT_SCALAR_A2A
        MODULE PROCEDURE FIELD_TO_CURRENT_UPSCALING_A2A
        MODULE PROCEDURE FIELD_TO_CURRENT_SCALAR_LIN
        MODULE PROCEDURE FIELD_TO_CURRENT_UPSCALING_LIN
    ENDINTERFACE

    INTERFACE CURRENT_TO_FIELD
        MODULE PROCEDURE CURRENT_TO_FIELD_SCALAR_A2A
        MODULE PROCEDURE CURRENT_TO_FIELD_UPSCALING_A2A
    ENDINTERFACE

CONTAINS
    FUNCTION  FIELD_TO_CURRENT_SCALAR_A2A(E,sigb,sqsigb,siga,Jc,Jpz,Jpx,Jpy,sqdz) RESULT(Ac)
        COMPLEX(REALPARM),INTENT(IN)::E(EX:,0:,0:,0:)
        COMPLEX(REALPARM),INTENT(IN)::sigb
        REAL(REALPARM),INTENT(IN)::sqsigb,sqdz
        COMPLEX(REALPARM),INTENT(IN)::siga
        INTEGER,INTENT(IN)::Jc
        INTEGER,INTENT(IN)::Jpx,Jpy,Jpz
        COMPLEX(REALPARM)::Ac
        COMPLEX(REALPARM)::asiga,dsig,gsig
        INTEGER::Px,Py,Pz
        INTEGER::Ix,Iy,Iz,Ic,I
        REAL(REALPARM)::S
        Pz=SIZE(E,2)-1
        Px=SIZE(E,3)-1
        Py=SIZE(E,4)-1
        S=CALC_SQR_NORM(Jpx,Jpy,Jpz)/sqdz
        asiga=C_TWO*sqsigb/(siga+CONJG(sigb))
        dsig=siga-sigb
        gsig=dsig*asiga
        Ac=E(Jc,Jpz,Jpx,Jpy)*gsig*S

    ENDFUNCTION
    FUNCTION  CURRENT_TO_FIELD_SCALAR_A2A(Ac,E0,sigb,sqsigb,siga,S) RESULT(E)
        COMPLEX(REALPARM),INTENT(IN)::Ac(EX:,0:,0:,0:)
        COMPLEX(REALPARM)::E0(Ex:,0:,0:,0:)
        COMPLEX(REALPARM),INTENT(IN)::sigb
        REAL(REALPARM),INTENT(IN)::sqsigb
        COMPLEX(REALPARM),INTENT(IN)::siga
        REAL(REALPARM),INTENT(IN)::S
        COMPLEX(REALPARM)::E(EX:EZ,0:SIZE(Ac,2)-1,0:SIZE(Ac,3)-1,0:SIZE(Ac,4)-1)
        COMPLEX(REALPARM)::asiga,d1(EX:EZ,0:SIZE(Ac,2)-1,0:SIZE(Ac,3)-1,0:SIZE(Ac,4)-1)
        COMPLEX(REALPARM)::d2(EX:EZ,0:SIZE(Ac,2)-1,0:SIZE(Ac,3)-1,0:SIZE(Ac,4)-1)
        REAL(REALPARM)::Sn
        INTEGER::Px,Py,Pz
        INTEGER::Ix,Iy,Iz,Ic
        Pz=SIZE(Ac,2)-1
        Px=SIZE(Ac,3)-1
        Py=SIZE(Ac,4)-1
        asiga=C_TWO*sqsigb/(siga+CONJG(sigb))
        DO Iy=0,Py
            DO Ix=0,Px
                DO Iz=0,Pz
                    Sn=CALC_SQR_NORM(Ix,Iy,Iz)/S
                    DO Ic=EX,EZ 
                        d1(Ic,Iz,Ix,Iy)=E0(Ic,Iz,Ix,Iy)*asiga
                        d2(Ic,Iz,Ix,Iy)=d1(Ic,Iz,Ix,Iy)-Ac(Ic,Iz,Ix,Iy)*Sn
                        E(Ic,Iz,Ix,Iy)=d2(Ic,Iz,Ix,Iy)*sqsigb
                    ENDDO
                ENDDO
            ENDDO
        ENDDO

    ENDFUNCTION

    FUNCTION  FIELD_TO_CURRENT_UPSCALING_A2A(E,sqsigb,GammaT,pnorm,sqdz) RESULT(Ac)
        COMPLEX(REALPARM),INTENT(IN)::E(EX:,0:,0:,0:)
        REAL(REALPARM),INTENT(IN)::sqsigb,sqdz,pnorm(:,:,:)
        COMPLEX(REALPARM),INTENT(IN)::GammaT(:,:)
        COMPLEX(REALPARM)::Ac(SIZE(E,2),SIZE(E,3),SIZE(E,4),3)
        COMPLEX(REALPARM)::asiga,dsig,gsig
        INTEGER::Px,Py,Pz
        INTEGER::N,Ic
        COMPLEX(REALPARM)::S
        Pz=SIZE(E,2)-1
        Px=SIZE(E,3)-1
        Py=SIZE(E,4)-1
        N=SIZE(GammaT,1)
        S=C_ONE/sqdz
        CALL ZGEMV('T',N,N,S,GammaT,N,E,ONE,C_ZERO,Ac,ONE)
        DO Ic=1,3
            Ac(:,:,:,Ic)=Ac(:,:,:,Ic)*pnorm
        ENDDO
    ENDFUNCTION

    FUNCTION  CURRENT_TO_FIELD_UPSCALING_A2A(Ac,E0,sqsigb,pnorm,GammaT,S) RESULT(E)
        COMPLEX(REALPARM),INTENT(IN)::Ac(1:,0:,0:,0:)
        COMPLEX(REALPARM)::E0(1:,0:,0:,0:)
        REAL(REALPARM),INTENT(IN)::sqsigb
        REAL(REALPARM),INTENT(IN)::pnorm(:,:,:)
        COMPLEX(REALPARM),INTENT(IN)::GammaT(:,:)
        REAL(REALPARM),INTENT(IN)::S
        COMPLEX(REALPARM)::E(1:3,0:SIZE(Ac,2)-1,0:SIZE(Ac,3)-1,0:SIZE(Ac,4)-1)
        COMPLEX(REALPARM)::ET(0:SIZE(Ac,2)-1,0:SIZE(Ac,3)-1,0:SIZE(Ac,4)-1,1:3)
        INTEGER::N,Ic
        COMPLEX(REALPARM)::S2
        DO Ic=1,3
            E(Ic,:,:,:)=pnorm*Ac(Ic,:,:,:)/S*sqsigb*C_TWO
        ENDDO

        E=E0-E
        N=SIZE(GammaT,1)
        ET=C_ZERO
        S2=-C_ONE/sqsigb
        CALL ZGEMV('T',N,N,S2,GammaT,N,E0,ONE,C_ONE,ET,ONE)
        DO Ic=1,3
            E(Ic,:,:,:)=E(Ic,:,:,:)+ET(:,:,:,Ic)
        ENDDO

    ENDFUNCTION

    FUNCTION  FIELD_TO_CURRENT_SCALAR_LIN(E,sigb,siga) RESULT(Ac)
        COMPLEX(REALPARM),INTENT(IN)::E(EX:,0:,0:,0:)
        COMPLEX(REALPARM),INTENT(IN)::sigb
        COMPLEX(REALPARM),INTENT(IN)::siga
        COMPLEX(REALPARM)::Ac(SIZE(E,2)*SIZE(E,3)*SIZE(E,4),3)
        COMPLEX(REALPARM)::dsig
        INTEGER::Px,Py,Pz
        INTEGER::Ix,Iy,Iz,Ic,Id
        Pz=SIZE(E,2)-1
        Px=SIZE(E,3)-1
        Py=SIZE(E,4)-1
        DO Ic=EX,EZ
            Id=1
            DO Iy=0,Py
                DO Ix=0,Px
                    DO Iz=0,Pz
                        dsig=siga-sigb
                        Ac(Id,Ic)=E(Ic,Iz,Ix,Iy)*dsig
                        Id=Id+1
                    ENDDO
                ENDDO
            ENDDO
        ENDDO

    ENDFUNCTION

    FUNCTION  FIELD_TO_CURRENT_UPSCALING_LIN(E,GammaT) RESULT(Ac)
        COMPLEX(REALPARM),INTENT(IN)::E(1:,0:,0:,0:)
        COMPLEX(REALPARM),INTENT(IN)::GammaT(:,:)
        COMPLEX(REALPARM)::Ac(SIZE(E,2)*SIZE(E,3)*SIZE(E,4),3)
        COMPLEX(REALPARM)::S
        INTEGER::N,Ng(2)
        N=SIZE(Ac)
        Ng=SHAPE(GammaT)
        S=C_TWO
        IF (ALL(Ng==N)) THEN
            CALL ZGEMV('T',N,N,S,GammaT,N,E,ONE,C_ZERO,Ac,ONE)
        ELSEIF (ALL(Ng==ONE)) THEN
            CALL ZCOPY(N,E,ONE,Ac,ONE)  
            Ac=Ac*GammaT(1,1)
        ELSE
            Ac=C_ZERO
        ENDIF
    ENDFUNCTION


    FUNCTION ALLOCATE_E_IE(ie_op) RESULT(Eint)
        TYPE(Operator_A2A),INTENT(IN)::ie_op
        COMPLEX(REALPARM),POINTER::Eint(:,:,:,:,:,:,:)
        INTEGER::Px,Py,Pz,Nx,Nz,Ny_loc
        Px=ie_op%col%Px
        Py=ie_op%col%Py
        Pz=ie_op%col%Pz
        Nx=ie_op%Nx
        Ny_loc=ie_op%Ny_loc
        Nz=ie_op%col%Nz
        IF (ie_op%real_space) THEN
            ALLOCATE(Eint(3,0:Pz,0:Px,0:Py,Nx,Ny_loc,Nz))
        ELSE
            Eint=>NULL()
        ENDIF
    ENDFUNCTION
    FUNCTION ALLOCATE_E_RC(rc_op) RESULT(Eint)
        TYPE(Operator_A2O),INTENT(IN)::rc_op
        COMPLEX(REALPARM),POINTER::Eint(:,:,:,:,:,:,:)
        INTEGER::Px,Py,Pz,Nx,Nz,Ny_loc
        Px=rc_op%col%Px
        Py=rc_op%col%Py
        Pz=rc_op%col%Pz
        Nx=rc_op%Nx
        Ny_loc=rc_op%Ny_loc
        Nz=rc_op%col%Nz
        IF (rc_op%real_space) THEN
            ALLOCATE(Eint(3,0:Pz,0:Px,0:Py,Nx,Ny_loc,Nz))
        ELSE
            Eint=>NULL()
        ENDIF
    ENDFUNCTION
    SUBROUTINE ALLOCATE_EH(rc_op,Ea,Ha) 
        TYPE(Operator_A2O),INTENT(IN)::rc_op
        COMPLEX(REALPARM),INTENT(INOUT),POINTER::Ea(:,:,:,:)
        COMPLEX(REALPARM),INTENT(INOUT),POINTER::Ha(:,:,:,:)
        IF (rc_op%real_space) THEN
            ALLOCATE(Ea(rc_op%Nr,EX:EZ,rc_op%Nx,rc_op%Ny_loc))
            ALLOCATE(Ha(rc_op%Nr,HX:HZ,rc_op%Nx,rc_op%Ny_loc))
        ELSE
            Ea=>NULL()
            Ha=>NULL()
        ENDIF
    ENDSUBROUTINE
ENDMODULE

