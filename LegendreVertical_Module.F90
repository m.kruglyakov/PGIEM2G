!Copyright (c) 2016 Mikhail Kruglyakov 
!This file is part of GIEM2G.
!
!GIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE LEGENDRE_VERTICAL_MODULE

    USE Const_module
    USE DATA_TYPES_MODULE
    USE PSI_MODULE
    USE FRAMES_AND_BOUNDARIES
    USE LEGENDRE_VERTICAL_OPTIONS_MODULE
    USE VERTICAL_MATCH_MODULE
    USE VERTICAL_MATCH_MODULE_V3
    USE SHIFTED_EXPONENT_MODULE
    IMPLICIT NONE
    PRIVATE


    PUBLIC :: OBTAIN_FIELDS_AT_SOURCE_DOMAIN
    PUBLIC :: OBTAIN_BOTTOM_FRAMES_FOR_IE_KERNEL
    PUBLIC :: OBTAIN_ALL_BOTTOM_FRAMES
    PUBLIC :: OBTAIN_TOP_FRAMES_FOR_IE_KERNEL
    PUBLIC :: OBTAIN_ALL_TOP_FRAMES
    PUBLIC :: CALCULATE_PSI, CALC_PSIMULT
CONTAINS



    SUBROUTINE OBTAIN_FIELDS_AT_SOURCE_DOMAIN(bkg,anomaly,expz,pexpz,qexpz,eta,Arr,Np,Psi,lm2,Ft,Fb,GII)
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),pexpz(2,anomaly%Nz),qexpz(2,anomaly%Nz)
        COMPLEX(REALPARM),INTENT(IN)::eta(bkg%Nl),Arr(bkg%Nl,2)
        INTEGER,INTENT(IN)::Np
        COMPLEX(REALPARM),INTENT(IN)::Psi(0:Np,anomaly%Nz)
        REAL(REALPARM),INTENT(IN)::lm2
        COMPLEX(REALPARM),INTENT(INOUT)::Ft(0:1,3,anomaly%Nz),Fb(0:1,anomaly%Nz),GII(0:Np,0:Np,4,anomaly%Nz)
        COMPLEX(REALPARM)::Ft2(0:1,3,anomaly%Nz),Fb2(0:1,anomaly%Nz)
        COMPLEX(REALPARM)::G2(0:Np,0:Np,4,anomaly%Nz)

        COMPLEX(REALPARM)::A(2),pqd(2)
        COMPLEX(REALPARM)::e(2),eta2,se(2)
        COMPLEX(REALPARM)::U(2,3),V(2,3)

        COMPLEX(REALPARM)::pe(2,3),qe(2,3)

        REAL(REALPARM)::h
        INTEGER::I,l
        INTEGER::Jp,Jn,Jm,Js,N0,N1

        DO I=1,anomaly%Nz
            l=anomaly%Lnumber(I)
            eta2=eta(l)*eta(l)
            A=Arr(l,:)
            e(1)=expz(I)
            e(2)=e(1)*e(1)
            h=anomaly%dz(I)
            se(1)=SHIFTED_EXP(-h*eta(l),e(1),BND_N)
            se(2)=SHIFTED_EXP(-h*eta(l)*R_TWO,e(2),BND_N)
            CALL OBTAIN_FIELD_AT_BOUNDARIES(h,e,se,pexpz(:,I),qexpz(:,I),eta(l),A,Ft(:,:,I),Fb(:,I))
            CALL  OBTAIN_MATCH_CELL_DOUBLE(Np,pexpz(:,I),qexpz(:,I),A,eta(l)*h,eta2,&
                &h,e,Psi(:,I),bkg%csigma(l),bkg%iwm,GII(:,:,:,I))

        ENDDO
    ENDSUBROUTINE

    SUBROUTINE OBTAIN_TOP_FRAMES_FOR_IE_KERNEL(bkg,anomaly,expz,eta,pexpz,fcont,t_frame)
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),eta(bkg%Nl),pexpz(2,anomaly%Nz)
        COMPLEX(REALPARM),INTENT(INOUT)::fcont(2,anomaly%Nz),t_frame(3,0:1,anomaly%Nz)
        COMPLEX(REALPARM)::a(2),b(2),c(2)
        COMPLEX(REALPARM)::a1(2),b1(2),a2(2),b2(2),c1(2),c2(2)
        COMPLEX(REALPARM)::U1(2),U2(2),V1(2),V2(2),H1(2),H2(2),H3(2)
        COMPLEX(REALPARM)::eta2
        COMPLEX(REALPARM)::kappa
        REAL(REALPARM)::dz
        INTEGER::I,Jp,l

        CALL OBTAIN_ALL_TOP_FRAMES(bkg,anomaly,ONE,expz,eta,pexpz,fcont,t_frame)
        !		DO I=1,anomaly%Nz-1
        !			l=anomaly%Lnumber(I
        !		!	t_frame(2:3,:,I)=t_frame(2:3,:,I)/bkg%csigma(l)
        !		ENDDO
    ENDSUBROUTINE

    SUBROUTINE OBTAIN_BOTTOM_FRAMES_FOR_IE_KERNEL(bkg,anomaly,expz,eta,qexpz,fcont,b_frame)
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),eta(bkg%Nl),qexpz(2,anomaly%Nz)
        COMPLEX(REALPARM),INTENT(INOUT)::fcont(anomaly%Nz),b_frame(0:1,anomaly%Nz)
        COMPLEX(REALPARM)::e,e2,t,a,a1,c1,f,se,se2
        INTEGER::I,l,Jp
        DO I=2,anomaly%Nz
            !DO I=anomaly%Nz,2,-1
            l=anomaly%Lnumber(I)
            e=expz(I)
            e2=e*e
            t=eta(l)*anomaly%dz(I)
            a=qexpz(2,I)

            c1=C_ONE+a*e2
            a1=C_ONE+a

            f=e*a1/c1
            a=a*e

            se=SHIFTED_EXP(-t,e,FRM_N)
            se2=SHIFTED_EXP(-t*R_TWO,e2,FRM_N)

            b_frame(0,I)=GET_BOTTOM_FRAME_P0D_1(e,e2,se,se2,t,a)
            b_frame(1,I)=GET_BOTTOM_FRAME_P1D_1(e,e2,se,se2,t,a)
            b_frame(:,I)=b_frame(:,I)/bkg%csigma(l)
            fcont(I)=f
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE OBTAIN_ALL_TOP_FRAMES(bkg,anomaly,N,expz,eta,pexpz,fcont,t_frame)
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        INTEGER,INTENT(IN)::N
        COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),eta(bkg%Nl),pexpz(2,anomaly%Nz)
        COMPLEX(REALPARM),INTENT(INOUT)::fcont(2,anomaly%Nz),t_frame(3,0:1,anomaly%Nz)
        COMPLEX(REALPARM)::a(2),b(2),c(2)
        COMPLEX(REALPARM)::a1(2),b1(2),a2(2),b2(2),c1(2),c2(2)
        COMPLEX(REALPARM)::e,e2,t,se,se2
        REAL(REALPARM)::dz
        INTEGER::I,Jp,l

        DO I=1,anomaly%Nz-N

            l=anomaly%Lnumber(I)
            dz=anomaly%dz(I)

            e=expz(I)
            e2=e*e
            t=eta(l)*dz
            a=pexpz(:,I)

            a1=C_ONE+a
            c1=C_ONE+a*e2

            se=SHIFTED_EXP(-t,e,FRM_N)
            se2=SHIFTED_EXP(-t*R_TWO,e2,FRM_N)


            t_frame(1,0,I)=GET_TOP_FRAME_P0D_0(e,e2,se,se2,t,a(1))*dz
            t_frame(2,0,I)=GET_TOP_FRAME_P0D_1(e,e2,se,se2,t,a(2))
            t_frame(3,0,I)=GET_TOP_FRAME_P0D_0(e,e2,se,se2,t,a(2))*dz

            t_frame(1,1,I)=GET_TOP_FRAME_P1D_0(e,e2,se,se2,t,a(1))*dz
            t_frame(2,1,I)=GET_TOP_FRAME_P1D_1(e,e2,se,se2,t,a(2))
            t_frame(3,1,I)=GET_TOP_FRAME_P1D_0(e,e2,se,se2,t,a(2))*dz

            fcont(:,I)=e*a1/c1
            t_frame(2:3,:,I)=t_frame(2:3,:,I)/bkg%csigma(l)
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE OBTAIN_ALL_BOTTOM_FRAMES(bkg,anomaly,N,expz,eta,qexpz,fcont,b_frame)
        TYPE (ANOMALY_TYPE),INTENT(IN)::anomaly
        TYPE (BKG_DATA_TYPE),INTENT(IN)::bkg
        INTEGER, INTENT(IN)::N
        COMPLEX(REALPARM),INTENT(IN)::expz(anomaly%Nz),eta(bkg%Nl),qexpz(2,anomaly%Nz)
        COMPLEX(REALPARM),INTENT(INOUT)::fcont(2,anomaly%Nz),b_frame(1:3,0:1,anomaly%Nz)
        COMPLEX(REALPARM)::a(2),b(2),c(2),eta2
        COMPLEX(REALPARM)::a1(2),b1(2),a2(2),b2(2),c1(2),c2(2)
        COMPLEX(REALPARM)::e,e2,t,se,se2
        REAL(REALPARM)::dz
        INTEGER::I,l,Jp
        DO I=N,anomaly%Nz

            l=anomaly%Lnumber(I)
            dz=anomaly%dz(I)

            e=expz(I)
            e2=e*e
            t=eta(l)*dz
            a=qexpz(:,I)

            a1=C_ONE+a
            c1=C_ONE+a*e2
            a=a*e

            se=SHIFTED_EXP(-t,e,FRM_N)
            se2=SHIFTED_EXP(-t*R_TWO,e2,FRM_N)

            b_frame(1,0,I)=GET_BOTTOM_FRAME_P0D_0(e,e2,se,se2,t,a(1))*dz
            b_frame(2,0,I)=GET_BOTTOM_FRAME_P0D_1(e,e2,se,se2,t,a(2))
            b_frame(3,0,I)=GET_BOTTOM_FRAME_P0D_0(e,e2,se,se2,t,a(2))*dz

            b_frame(1,1,I)=GET_BOTTOM_FRAME_P1D_0(e,e2,se,se2,t,a(1))*dz
            b_frame(2,1,I)=GET_BOTTOM_FRAME_P1D_1(e,e2,se,se2,t,a(2))
            b_frame(3,1,I)=GET_BOTTOM_FRAME_P1D_0(e,e2,se,se2,t,a(2))*dz

            b_frame(2:3,:,I)=b_frame(2:3,:,I)/bkg%csigma(l)
            fcont(:,I)=expz(I)*a1/c1
        ENDDO
    ENDSUBROUTINE

    SUBROUTINE CALC_PSIMULT(kappa,expz,Np, Psi)
        COMPLEX(REALPARM),INTENT(IN)::kappa
        COMPLEX(REALPARM),INTENT(IN)::expz
        INTEGER,INTENT(IN)::Np
        COMPLEX(REALPARM),INTENT(INOUT)::Psi(2:Np)
        INTEGER::I,N
        IF (Np<2) RETURN
        CALL CALCULATE_PSI(Np,kappa,expz,Psi)
        DO I=4,Np
            Psi(I)=Psi(I-2)*Psi(I)
        ENDDO
    ENDSUBROUTINE




ENDMODULE
