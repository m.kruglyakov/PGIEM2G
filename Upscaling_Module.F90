!Copyright (c) 2017 Mikhail Kruglyakov 
!This file is part of PGIEM2G.
!
!PGIEM2G is free software: you can redistribute it and/or modify
!it under the terms of the GNU General Public License as published by
!the Free Software Foundation, either version 2 of the License.
!
!GIEM2G is distributed in the hope that it will be useful,
!but WITHOUT ANY WARRANTY; without even the implied warranty of
!MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!GNU General Public License for more details.
!
!You should have received a copy of the GNU General Public License
!along with GIEM2G.  If not, see <http://www.gnu.org/licenses/>.
!
!
!

MODULE	UPSCALING_MODULE 
	USE CONST_MODULE
	USE OPERATORS_MODULE
	USE POLYNOMIAL_UTILITIES_MODULE
	USE Timer_Module 
	USE PGIEM2G_LOGGER_MODULE

	IMPLICIT NONE
	INTERFACE FILL_GAMMAT
		MODULE PROCEDURE FILL_GAMMAT_IE, FILL_GAMMAT_RC, FILL_GAMMAT_AR
	ENDINTERFACE
CONTAINS

	SUBROUTINE FILL_GAMMAT_IE(ie_op)
		TYPE (OPERATOR_A2A),INTENT(INOUT)::ie_op
		INTEGER::Ix,Iy,Iz,N
		IF (ie_op%real_space) THEN
			IF  (ASSOCIATED(ie_op%GammaT)) DEALLOCATE(ie_op%GammaT)
			N=3*(ie_op%col%Px+1)*(ie_op%col%Py+1)*(ie_op%col%Pz+1)
			ALLOCATE(ie_op%GammaT(N,N,ie_op%col%Nz,ie_op%Nx,ie_op%Ny_loc))
			CALL FILL_GAMMAT_AR(ie_op%Nx,ie_op%Ny_loc,ie_op%col,&
				  &ie_op%csiga,ie_op%csigb,ie_op%GammaT)
		ENDIF
	ENDSUBROUTINE

	SUBROUTINE FILL_GAMMAT_RC(rc_op)
		TYPE (OPERATOR_A2O),INTENT(INOUT)::rc_op
		INTEGER::Ix,Iy,Iz,N
		IF (rc_op%real_space) THEN
			IF  (ASSOCIATED(rc_op%GammaT)) DEALLOCATE(rc_op%GammaT)
			N=3*(rc_op%col%Px+1)*(rc_op%col%Py+1)*(rc_op%col%Pz+1)
			ALLOCATE(rc_op%GammaT(N,N,rc_op%col%Nz,rc_op%Nx,rc_op%Ny_loc))
			CALL FILL_GAMMAT_AR(rc_op%Nx,rc_op%Ny_loc,rc_op%col,&
				  &rc_op%csiga,rc_op%csigb,rc_op%GammaT)
		ENDIF
	ENDSUBROUTINE
	SUBROUTINE FILL_GAMMAT_AR(Nx,Ny_loc,col,csiga,csigb,GammaT)
		INTEGER::Nx,Ny_loc
		TYPE(VERTICAL_DOMAIN),INTENT(IN)::col
		COMPLEX(REALPARM),INTENT(IN)::csiga(:,:,:),csigb(:)
		COMPLEX(REALPARM),INTENT(INOUT)::GammaT(:,:,:,:,:)
		INTEGER::Ix,Iy,Iz,N
		TYPE(POL_ORDER_TYPE)::P
		P=CREATE_POL_DATA(col%Px,&
				 &col%Py,&
				 &col%Pz)
		DO Iy=1,Ny_loc
			DO Ix=1,Nx
				DO Iz=1,col%Nz
					CALL CREATE_DIAGONAL_TENSOR(&
					&csiga(Ix,Iy,Iz),csigb(Iz),P,&
					&col%pnorm,&
					&GammaT(:,:,Iz,Ix,Iy))
				ENDDO
			ENDDO
		ENDDO
	ENDSUBROUTINE

	SUBROUTINE CREATE_DIAGONAL_TENSOR(siga,sigb,P,pnorm,GammaT)
		COMPLEX(REALPARM),INTENT(IN)::siga,sigb
		TYPE(POL_ORDER_TYPE),INTENT(IN)::P
		REAL(REALPARM),INTENT(IN)::pnorm(0:,0:,0:)
		COMPLEX(REALPARM),INTENT(INOUT)::GammaT(0:,0:)
		INTEGER::Ix,Iy,Iz
		INTEGER::Ic
		INTEGER::Isrc,Irecv
		INTEGER::Nxz,Nxyz
		REAL(REALPARM)::sqsig
		Nxz=(P%Nz+1)*(P%Nx+1)
		Nxyz=Nxz*(P%Ny+1)
		GammaT=C_ZERO
		sqsig=SQRT(REAL(sigb))
		DO Ic=0,2
			DO Iy=0,P%Ny
				DO Ix=0,P%Nx
					DO Iz=0,P%Nz
						Irecv=Iz+Ix*(P%Nz+1)+&
						&Iy*Nxz+Ic*Nxyz

						Isrc=Ic+Iz*3+Ix*(P%Nz+1)*3+&
						&Iy*Nxz*3
						
						GammaT(Isrc,Irecv)=sqsig*&
						&(siga-sigb)/(siga+CONJG(sigb))
					ENDDO
				ENDDO
			ENDDO 
		ENDDO 
	ENDSUBROUTINE

ENDMODULE
